var noble = require('../index');
var serviceUUIDs =  ["fff0"];
var charUUIDs = ["fff2"] ;  

var  calculateChecksum = function(data)
{
        var checksum = 0;
		for (var i = 0; i < data.length-2; i++)
			checksum += data[i];
		
        data[data.length-2] = ((checksum & 0xff00) >> 8);
        data[data.length-1] = (checksum & 0x00ff);
        return data;
};
// creating data to ask for handshake
var arr = new Uint8Array(20);
arr[0] = 26;
//
arr[1] = 1;
arr[2] = 0;
arr[3] = 1;

arr[4] = 0;
arr[5] = 0;
arr[6] = 0;
arr[7] = 0;
arr[8] = 0;

arr[9] = 0;
arr[10] = 0;
arr[11] = 0;
arr[12] = 0;

arr[13] = 0;
arr[14] = 0;
arr[15] = 0;
arr[16] = 0;
// arr[17] = 0;
// arr[18] = 0;

var handshakePIN = calculateChecksum(arr);

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning(serviceUUIDs, false);
  } else {
    noble.stopScanning();
  }
});


noble.on('discover', function(peripheral) {

	if(peripheral.advertisement.localName == undefined || peripheral.advertisement.localName.indexOf("MECCANO") == -1){
		return;
	}
  console.log('peripheral discovered (' + peripheral.id +
              ' with address <' + peripheral.address +  ', ' + peripheral.addressType + '>,' +
              ' connectable ' + peripheral.connectable + ',' +
              ' RSSI ' + peripheral.rssi + ':');
  console.log('\thello my local name is:');
  console.log('\t\t' + peripheral.advertisement.localName);
  console.log('\tcan I interest you in any of the following advertised services:');
  console.log('\t\t' + JSON.stringify(peripheral.advertisement.serviceUuids));

  var serviceData = peripheral.advertisement.serviceData;
  if (serviceData && serviceData.length) {
    console.log('\there is my service data:');
    for (var i in serviceData) {
      console.log('\t\t' + JSON.stringify(serviceData[i].uuid) + ': ' + JSON.stringify(serviceData[i].data.toString('hex')));
    }
  }
  if (peripheral.advertisement.manufacturerData) {
    console.log('\there is my manufacturer data:');
    console.log('\t\t' + JSON.stringify(peripheral.advertisement.manufacturerData.toString('hex')));
  }
  if (peripheral.advertisement.txPowerLevel !== undefined) {
    console.log('\tmy TX power level is:');
    console.log('\t\t' + peripheral.advertisement.txPowerLevel);
  }
 
  //noble.stopScanning();

    peripheral.connect(function(error){
   	
   		console.log('connected?!')
   		console.log(error);

   	// discover services - works

      // 	var dServices = peripheral.discoverServices( [],
	  // 	function(error, services){
	  // 		console.log("discoverServices CB")
	  // 		console.log(error, services)
	  // 	}
  	  // );
		 // console.log(dServices);
		 // var serviceUUIDs = ["<service UUID 1>", ...];
	
		peripheral.discoverSomeServicesAndCharacteristics(serviceUUIDs, charUUIDs, function(error, services, characteristics){
			console.log(characteristics)
			var buffer = new Buffer(20);

			for (var i = 19; i >= 0; i--) {
				buffer.writeUInt8(handshakePIN[i], i);
				// buffer[i]
			}
  			
			console.log(buffer);

			characteristics[0].write(buffer, false, function(error){
					// console.log("error is", error)
			}); 
			// var req = characteristics[0]
			// 	console.log("req ", req);

		});

   });
  
});

// function callback(error, services){

// 	console.log(error, services)
// }