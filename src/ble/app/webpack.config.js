var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('../node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    console.log(mod)
    if(mod == 'bluetooth-hci-socket' || mod == 'xpc-connection' ||
       mod == 'ws' || mod == 'express-promise'){
      nodeModules[mod] = 'commonjs ' + mod;
    }
  });

module.exports = {
  entry: './app.js',
  target:'node',
  output: {
    filename: 'ble-server-bundle.js',
    path: path.resolve(__dirname, '')
  },
  node: {
  	fs: "empty",
  	net: "mock"
	},
  module:  {
    exprContextCritical: false
  },
  externals:nodeModules,

};
