
exports.ziv = "WAKAWAKa";
exports.calculateChecksum = function(data){
        var checksum = 0;
		for (var i = 0; i < data.length-2; i++)
			checksum += data[i];

        data[data.length-2] = ((checksum & 0xff00) >> 8);
        data[data.length-1] = (checksum & 0x00ff);
        return data;
};

exports.bufferize = function(arr, bufferLength){

	if( typeof(bufferLength) == 'undefined'){
		bufferLength = 20
	}
	
	var buffer = new Buffer(bufferLength);

	for (var i = bufferLength-1; i >= 0; i--) {

		buffer.writeUInt8(arr[i], i);
				// buffer[i]
	}
	return buffer;
}

exports.uIntArrFromStr = function (str){

	var strArr = str.split(",")
	var uIntArr = new Uint8Array(20)
	for (var i = 0; i < 16; i++) {
		uIntArr[i] = parseInt(strArr[i])
	}
	return uIntArr;
}

exports.commandFromStr = function (str){

	var strArr = str.split(",")
	return strArr[0]
}
// var robotCo

// // FOR DEBUG ONLY::
var robotCommands = {};
// handshake command
var handshake = new Uint8Array(20);
handshake[0] = 26;
handshake[1] = 1;
handshake[2] = 0;
handshake[3] = 1;
handshake[4] = 0;
handshake[5] = 0;
handshake[6] = 0;
handshake[7] = 0;
handshake[8] = 0;
handshake[9] = 0;
handshake[10] = 0;
handshake[11] = 0;
handshake[12] = 0;
handshake[13] = 0;
handshake[14] = 0;
handshake[15] = 0;
handshake[16] = 0;

handshake = exports.calculateChecksum(handshake);
robotCommands.Handshake = exports.bufferize(handshake);

// checkstatus command
var checkstatus = new Uint8Array(20);
checkstatus[0] = 1;
checkstatus[1] = 0;
checkstatus[2] = 0;
checkstatus[3] = 0;
checkstatus[4] = 0;
checkstatus[5] = 0;
checkstatus[6] = 0;
checkstatus[7] = 0;
checkstatus[8] = 0;
checkstatus[9] = 0;
checkstatus[10] = 0;
checkstatus[11] = 0;
checkstatus[12] = 0;
checkstatus[13] = 0;
checkstatus[14] = 0;
checkstatus[15] = 0;
checkstatus[16] = 0;

checkstatus = exports.calculateChecksum(checkstatus);
robotCommands.CheckStatus = exports.bufferize(checkstatus);

var servoColor = new Uint8Array(20);
servoColor[0] = 12;
//
servoColor[1] = 2;
servoColor[2] = 2;
servoColor[3] = 2;
servoColor[4] = 2;
servoColor[5] = 2;
servoColor[6] = 2;
servoColor[7] = 2;
servoColor[8] = 0;
servoColor[9] = 0;
servoColor[10] = 0;
servoColor[11] = 0;
servoColor[12] = 0;
servoColor[13] = 0;
servoColor[14] = 0;
servoColor[15] = 0;
servoColor[16] = 0;

servoColor = exports.calculateChecksum(servoColor);
robotCommands.ServoColor = exports.bufferize(servoColor);

var servoColor2 = new Uint8Array(20);
servoColor2[0] = 12;
//
servoColor2[1] = 4;
servoColor2[2] = 4;
servoColor2[3] = 4;
servoColor2[4] = 4;
servoColor2[5] = 3;
servoColor2[6] = 3;
servoColor2[7] = 3;
servoColor2[8] = 0;
servoColor2[9] = 0;
servoColor2[10] = 0;
servoColor2[11] = 0;
servoColor2[12] = 0;
servoColor2[13] = 0;
servoColor2[14] = 0;
servoColor2[15] = 0;
servoColor2[16] = 0;

servoColor2 = exports.calculateChecksum(servoColor2);
robotCommands.ServoColor2 = exports.bufferize(servoColor2);


var setservo = new Uint8Array(20);
setservo[0] = 8;
setservo[1] = 1;
setservo[2] = 1;
setservo[3] = 1;
setservo[4] = 1;
setservo[5] = 1;
setservo[6] = 1;
setservo[7] = 1;
setservo[8] = 1;
setservo[9] = 1;
setservo[10] = 1;
setservo[11] = 1;
setservo[12] = 1;
setservo[13] = 1;
setservo[14] = 1;
setservo[15] = 1;
setservo[16] = 1;

setservo = exports.calculateChecksum(setservo);
robotCommands.setservo = exports.bufferize(setservo);


var setservo2 = new Uint8Array(20);
setservo2[0] = 8;
setservo2[1] = 0;
setservo2[2] = 0;
setservo2[3] = 0;
setservo2[4] = 0;
setservo2[5] = 0;
setservo2[6] = 0;
setservo2[7] = 0;
setservo2[8] = 0;
setservo2[9] = 0;
setservo2[10] = 0;
setservo2[11] = 0;
setservo2[12] = 0;
setservo2[13] = 0;
setservo2[14] = 0;
setservo2[15] = 0;
setservo2[16] = 0;

setservo2 = exports.calculateChecksum(setservo2);
robotCommands.setservo2 = exports.bufferize(setservo2);


exports.robotCommands = robotCommands;

