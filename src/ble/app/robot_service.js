

// instances of robot and charcteristics
var robot = null;
var mainCharacteristic = false;
var subCharacteristic = false;
var mainService = false;
var configService = false; 
var configCharacteristic = false;

var robotName = null

var possibleNames = []
var possibleRobots = {}

var commandsCache = {}
// TODO SHOULD NOT BE IN NODE ?
var bleUtils = require('./bleutils');
let robotCommands = bleUtils.robotCommands;

let logModule = require('./ble_logger')
const log = logModule.bLog

var bluetoothWorking = false
var isSearching = false;
var RobotService = {
	currentRobotQueries:{}
};
let isWin = /^win/.test(process.platform);

RobotService.isSafeToPoll = function(){
	// console.log("isSafeToPoll called, " + Object.keys(commandsCache).length+ " commands in cache" )
  	return Object.keys(commandsCache).length == 0;
};
var noble = require('../index')
// var serviceUUIDs =  ["fff0"]//, "f000ccc0-0451-4000-b000-000000000000"];
// var charUUIDs = ["fff2", "fff1"]//, "f000ccc2-0451-4000-b000-000000000000"];
// minimum delay after a ble call
var delay = isWin ? 111 : 111;

// creating data to ask for handshake

noble.on('stateChange', function(state) {
 if (state === 'poweredOn') {
	 bluetoothWorking = true
 } else {
	 bluetoothWorking = false
 }
});


noble.on('discover', function(peripheral) {

	if (peripheral.advertisement.localName == undefined 
		|| (peripheral.advertisement.localName.indexOf("MECCANO") == -1 
	   		&& peripheral.advertisement.localName.indexOf("MAX") == -1
	   		&& peripheral.advertisement.localName.indexOf("MeccaSpider") == -1)) {
		return;
	}

	log.info('peripheral discovered (' + peripheral.id +
	          ' with address <' + peripheral.address +  ', ' + peripheral.addressType + '>,' +
	          ' connectable ' + peripheral.connectable + ',' +
	          ' RSSI ' + peripheral.rssi + ':');
	log.info('\thello my local name is:');
	log.info('\t\t' + peripheral.advertisement.localName);
	log.info('\tcan I interest you in any of the following advertised services:');
	log.info('\t\t' + JSON.stringify(peripheral.advertisement.serviceUuids));

	var serviceData = peripheral.advertisement.serviceData;
	if (serviceData && serviceData.length) {
		// log.info('\there is my service data:');
	for (var i in serviceData) {
		// log.info('\t\t' + JSON.stringify(serviceData[i].uuid) + ': ' + JSON.stringify(serviceData[i].data.toString('hex')));
	}
	}
	if (peripheral.advertisement.manufacturerData) {
		// log.info('\there is my manufacturer data:');
		// log.info('\t\t' + JSON.stringify(peripheral.advertisement.manufacturerData.toString('hex')));
	}
	if (peripheral.advertisement.txPowerLevel !== undefined) {
		// log.info('\tmy TX power level is:');
		// log.info('\t\t' + peripheral.advertisement.txPowerLevel);
	}
	if(possibleNames.indexOf(peripheral.advertisement.localName) == -1){

		possibleRobots[peripheral.advertisement.localName] = peripheral
		possibleNames.push(peripheral.advertisement.localName)
		log.info('pushed robot to array')
	}


});

// api methods, accesible from app.js

RobotService.findRobot = function(){

		// RobotService.disconnect()

	if(null == robot){// && !isSearching ){
      	// log.info('starting scan !')
      	isSearching = true
    	// noble.startScanning(serviceUUIDs, false);

    	noble.startScanning();
  }else{
      	log.info('no need to scan !')
  }
}


RobotService.writeThrottled = function(cmd, buffer){

	// log.info(cmd, buffer, commandsCache[cmd])
	if(!robot || robot.state == 'disconnected'){
		log.info("no queing, no connection");
		return;
	}

	commandsCache[cmd] = buffer

}

setInterval(function(){

	if( Object.keys(commandsCache).length >= 3){

		log.info("WARNING! got " + Object.keys(commandsCache).length +" commands in cache" );
	}

	for (var cmd in commandsCache) {

	    if (commandsCache.hasOwnProperty(cmd) && robot) {

	        if (RobotService.writeToRobot(commandsCache[cmd])){

            	delete commandsCache[cmd]
          	}
	        break;
	    }
	}
},delay)

RobotService.writeToRobot = function(buffer){

  // check that robot is connected
	if(!robot || !mainCharacteristic || robot.state != 'connected'){
	
		log.info("not writing, no connection");
		log.info(buffer);
		if(robot){
			log.info(robot.state)
		}
		return false;
	}

  	// log.info(' ROBO STATE:: '+ robot.state + ", command:  " +buffer[0]);
  	// console.log("writing (in app code), command is " + buffer[0]+ "  " + Date.now())
  	mainCharacteristic.write(buffer, true, function(error){
  		// "characteristic.write" is throwing errors all the time regardless if neccesary
  		// see: https://github.com/sandeepmistry/noble/blob/master/lib/characteristic.js line 70
  		if (error !== null){
  			log.info(mainCharacteristic)
	  		log.info("error when writing command "+JSON.stringify(buffer)+", write error is "+ JSON.stringify(error))
	  	}
	});
  return true;

}

RobotService.discover = function(){

	robot.discoverSomeServicesAndCharacteristics(serviceUUIDs, charUUIDs, function(error, services, characteristics){

		// mainCharacteristic = characteristics[0];

		characteristics[0].read ( function(e, data){

			// log.info(data.toString('utf8'))
		})
		// 	log.info("req ", req);

	})
}

RobotService.subscribe = function(listenToMe){
	// robot.discoverSomeServicesAndCharacteristics(serviceUUIDs,['fff1'], function(error, services, characteristics){
	if(!robot || robot.state !="connected"){
		if(robot){
			log.info(robot.state)
		}
		return
	}
	subCharacteristic.subscribe()
	subCharacteristic.on('data', listenToMe)
}

RobotService.disconnect = function(){

	if(null != robot){

		robot.disconnect(function(error) {
	       // log.info('disconnected from peripheral: ' + robot.uuid);
			resetRobot()
	    })
	}else{ // maybe we have some leftovers from bad disconnection
		resetRobot()
	}
}

function resetRobot(){
	log.info('resetRobot')
    robot = null;
	mainCharacteristic = null
  if (subCharacteristic){

    subCharacteristic.unsubscribe()
  }
	subCharacteristic = null
	mainService = null
	robotName = null

	possibleNames = []
	possibleRobots = {}
	commandsCache = {}
}

RobotService.getConnectionStatus = function(){

	return (null !== robot)
}
RobotService.selectByName = function(name){

	connect(name)
}

RobotService.getConnectionStatusObj = function(){

	if(null !== robot){
		return {status:'selected', name:robotName}
	}
	if(possibleNames.length > 0){
		return {status:'found_robots', robotList:  possibleNames}
	}
	return {status: (isSearching ? 'searching' : 'not_searching')}

}

RobotService.isBLEPowered = function(){
	return bluetoothWorking
}

RobotService.reConfigureLatency = function(){

	if (isWin){

		return;
	}
	let arr = new Uint8Array(8);
      // arr[0] = 0x18;     
    arr[0] = 0x10;      

    arr[1] = 0x00;
    arr[2] = 0x10;
    arr[3] = 0x00;      
    arr[4] = 0x00;
    arr[5] = 0x00;
    arr[6] = 0x64;
    arr[7] = 0x00;

    let buffer = bleUtils.bufferize(arr, 8);
    configCharacteristic.write(buffer, false, function(error){
  		
  		log.info("error when trying to reconfigure latency write error is ", error);
	});
}

var connect = function(name){
// var connect = function(){

	noble.stopScanning();
  	isSearching = false
	// robot = peripheral;

	log.info('trying to connect');
	var r = possibleRobots[name] ;
	r.on('disconnect', function() {
		//  reinitialize
		resetRobot()
		// robot = null;
		// mainCharacteristic = null
		// subCharacteristic = null
		// mainService = null
		// robotName = null
    //
		// possibleNames = []
		// possibleRobots = {}
		// commandsCache = {}
		// noble.startScanning(serviceUUIDs, false);
    });
	r.connect(function(error){

		// r.discoverSomeServicesAndCharacteristics(serviceUUIDs, charUUIDs, function(error, services, characteristics){
		// 	console.log(services)
		// 	console.log(characteristics)

		// 	robot = r
		// 	robotName = name
		// 	mainService = services[0]
		// 	mainCharacteristic = characteristics[0]
		// 	subCharacteristic = characteristics[1]
		// });

		r.discoverAllServicesAndCharacteristics(function(error, services, characteristics){
			// console.log(services)
			// console.log(characteristics)

			robot = r
			robotName = name

			for (var i = services.length - 1; i >= 0; i--) {
				
				if(services[i].uuid == 'fff0'){
				
					mainService = services[i]
					// console.log("found main service in index "+i)
				}
				if(services[i].uuid == 'f000ccc004514000b000000000000000'){
				
					configService = services[i]
					// console.log("found config service in index "+i)
				}
			}
			for (var i = characteristics.length - 1; i >= 0; i--) {
				
				if(characteristics[i].uuid == 'fff2'){
				
					mainCharacteristic = characteristics[i]
					// console.log("found main characteristic in index "+i)
				}
				if(characteristics[i].uuid == 'fff1'){
				
					subCharacteristic = characteristics[i]
					// console.log("found subscribe characteristic in index "+i)
				}
				if(characteristics[i].uuid == 'f000ccc204514000b000000000000000'){
				
					configCharacteristic =  characteristics[i]
					// console.log("found config characteristic in index "+i)
				}
			}

			if(!mainService || !configService || !mainCharacteristic || !subCharacteristic || !configService){

				log.info("ERROR! An Important GATT Service or GATT Characteristic was not found!!!");
			}
			
		});
	});
}
RobotService.delay = delay
RobotService.robotName = robotName

exports.RobotService = RobotService
