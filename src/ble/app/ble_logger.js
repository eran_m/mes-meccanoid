// LOGGER FOR NODE BLE SERVICE

var fs = require('fs');
let isWin = /^win/.test(process.platform);
let dir = ''
var rollingLogger

if( isWin ){

  dir = process.env.APPDATA + '\\SpinMaster\\'
}else{

  dir =  (process.platform == 'darwin' ? process.env.HOME + '/Library/Logs' : '/var/local') + '/SpinMaster'
}
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
// process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + 'Library/Preferences' : '/var/local')
// create a rolling file logger based on date/time
// console.log('log dir is: '+ dir  +"!")

const opts = {
    logDirectory:dir ,
    fileNamePattern:'roll-<DATE>.log',
    dateFormat:'YYYY.MM.DD'
};

rollingLogger = require('simple-node-logger').createRollingFileLogger( opts )

var bleLogger = {
	info : function(msg){
		// console.log("INFO : " + msg)
		rollingLogger.info(msg)
	}
}


exports.bLog = bleLogger
