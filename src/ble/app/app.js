let logModule = require('./ble_logger')
const log = logModule.bLog
const MB_GetServoPos = "9";
const QUERY_CACHE_MS = 1111;

process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  console.error(err.stack)
  log.info(err.stack)
})

// override native js console for production
// var console = {};
// console.log = function(d){log.info(d)};
// console.warn = function(d){log.info(d)};
// console.error = function(d){log.info(d)};

// try {
  // log.info("trying to require...")
  var express = require('express')
  var app = express()
  var fs = require('fs')
  var path = require('path')
  var bleUtils = require('./bleutils')
  var serviceModule = require('./robot_service')

// } catch (e) {
//   log.info("unhandled error in requires... error was : " + e.message)
//   // process.exit(1)
// }


let RobotService = serviceModule.RobotService
let robotCommands = bleUtils.robotCommands


var parentDir = path.resolve(process.cwd());
// console.log(parentDir)

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // console.log('in headers ', res)
  next();
});
// // to serve our xml staticly to client
// app.use(express.static('.\\meccanoid'))
// for promises...
app.use(require('express-promise')());

// keys will be added/removed dynamically as responses are returned from robot
var responsesFromRobot = {
  // "1" : [byte array which is a result of call to robot]
};
app.get('/alive', function (req, res, next) {

    res.send({
      status: true,
      msg:    'serverrr active',
      ble:    RobotService.isBLEPowered() ? "on" : "off"
    })
})

app.get('/initialize', function (req, res, next) {
    log.info('init was called in app')
    log.info('robot:')
    log.info(RobotService.getConnectionStatusObj())

    RobotService.findRobot()
    res.send({status:true,msg:'finding robot...'})
})

app.get('/write', function (req, res, next) {

    log.info('write was called in app with data: ' + req.query.d)
    log.info('robot:')
    log.info(RobotService.getConnectionStatusObj())


     var cmd = bleUtils.commandFromStr(req.query.d)
    // console.log(req.query)
    // translate string from req to int array
    var uArr = bleUtils.uIntArrFromStr(req.query.d)
    // add checksum
    uArr = bleUtils.calculateChecksum(uArr)
    // put in buffer and write to characteristic

    RobotService.writeThrottled(cmd,bleUtils.bufferize(uArr));

    res.json({status:true,msg:
      'wrote to robot'})

})

app.get('/write_with_response_force', function (req, res, next) {
    
    var result = writeWithResponse(req.query.d, true)
    
    res.json(result)
})
// wait for response to question we asked robot

app.get('/write_with_response_weak', function (req, res, next) {
    
    var result = writeWithResponse(req.query.d, false)
    // promise ...
    res.json(result)
})

// keep state of object representing responses from robot to app

function updateQueriesIngoing(data){
    

    var cmd = data[0].toString();
    if (typeof(RobotService.currentRobotQueries[cmd]) === 'undefined' || !RobotService.currentRobotQueries[cmd].live){
      // log.info("??WTF?? "+ cmd);
      // normally we don't expect this to happen, except for "26" handshake command
      return;
    }
    // set state of responses object, refresh last response data that will be returned as cache
    // log.info('updateQueriesIngoing cmd '+ cmd + ' result after: '+((Date.now() - RobotService.currentRobotQueries[cmd].lastCallToRobot)/1000) +"s")
    RobotService.currentRobotQueries[cmd].data = data
    RobotService.currentRobotQueries[cmd].live = false;

}

// keep state of object representing responses from robot to app

function updateQueriesOutgoing(cmd, force){
  // a new query 
  if (typeof(RobotService.currentRobotQueries[cmd]) === 'undefined'){
    
    // log.info(cmd +' called first time. ');
    RobotService.currentRobotQueries[cmd] = {
      lastCallToRobot: Date.now(),
      data:"",
      live:true,
      timesTried:1,      
    };    
    return true;
  // not new..
  }else{
  
    let timeElapsed = Date.now() - RobotService.currentRobotQueries[cmd].lastCallToRobot;
    // are we currently waiting for a response?
    if( RobotService.currentRobotQueries[cmd].live ){

      RobotService.currentRobotQueries[cmd].timesTried += 1;
      // log.info(cmd +' called again while live. elapsed '+ (timeElapsed) + 'ms, tried '+ RobotService.currentRobotQueries[cmd].timesTried + ' times');

      // we are, determine whether we should stop waiting
      return (timeElapsed > (QUERY_CACHE_MS));
    }else{
      // we aren't, set state of responses object 
      RobotService.currentRobotQueries[cmd].live = true
      RobotService.currentRobotQueries[cmd].lastCallToRobot = Date.now()
      RobotService.currentRobotQueries[cmd].timesTried = 1;

      // log.info(cmd +' called. ');
      return true;
    }
  }
}

function writeWithResponse(commandStr, force){

    var cmd = bleUtils.commandFromStr(commandStr)
    
    // translate string from req to int array
    var uArr = bleUtils.uIntArrFromStr(commandStr)
    // add checksum
    uArr = bleUtils.calculateChecksum(uArr)
    // console.log("writeWithResponse called, cmd is " +cmd+ ", " + (force ? "active":"passive") + " polling")
    var result = {status:true, message: cmd + " was not called, robot busy or not Rconnected"};
    
    // for now only "read servos" command can be returned from cache
    if (!updateQueriesOutgoing(cmd, force) && cmd == MB_GetServoPos){
      // log.info('cached response for command '+ cmd+ ', '+JSON.stringify(RobotService.currentRobotQueries))
      result.message = "cache"
      result.data = RobotService.currentRobotQueries[cmd].data;
      return result;
    }


    if ( RobotService.isSafeToPoll() || force == true) {

      RobotService.writeThrottled(cmd,bleUtils.bufferize(uArr));
      result = pollForCommand(cmd)
    }
    return result;
}

app.get('/select_robot', function (req, res, next) {
    var name = bleUtils.commandFromStr(req.query.d)
    // translate string from req to int array
    // var uArr = bleUtils.uIntArrFromStr(req.query.d)
    // // add checksum
    log.info('select called')
    log.info('name ' +name)
    log.info('robot:')
    log.info(RobotService.getConnectionStatusObj())


    // uArr = bleUtils.calculateChecksum(uArr)
    RobotService.selectByName(name)

    // promise ...
    res.json({status:'connecting'})

})

app.get('/kill', function (req, res, next) {
  process.exit()
})

app.get('/connection_check', function (req, res, next) {

    // res.json({status: RobotService.getConnectionStatus()})
    res.json(RobotService.getConnectionStatusObj())

})

app.get('/disconnect_robot', function (req, res, next) {

    RobotService.disconnect()
    // res.json({status: RobotService.getConnectionStatus()})
    res.json({})

})

// setup listener to responses from robot
app.get('/subscribe', function (req, res) {

    var result = RobotService.subscribe(callBackFromSubscribe)
    // log.inforesult);
    res.send({status:true,msg:'trying to subscribe' })

})

app.get('/configure_latency', function (req, res) {

    RobotService.reConfigureLatency()
    res.send({status:true,msg:'reconfiguring robot latency' })

})


app.listen(1234, function () {
  log.info("BLE server loaded successfully! delay is " + RobotService.delay);
})


// command will be a uInt cast as string
function pollForCommand(command){

    // polling i time limited
    var keepPolling = true
    var startTs = + new Date()
    var result ={status:false, message: command + " pollForCommand timed out w/o response from robot"}

    while(keepPolling){
      var currTs = + new Date()
      if(( currTs - startTs ) > (50)){
          keepPolling = false;
          //for debug
          // responsesFromRobot[command] = [0,60,7,3]
          // break;
      }
      if(responsesFromRobot.hasOwnProperty(command) ){

          result.data = responsesFromRobot[command]
          result.status = true
          result.name = RobotService.robotName
          result.message = command + ": got response for robot "
          delete responsesFromRobot[command]
          break;
      }
    }

    return result;
}

// just set reponses object according to result first byte (= command)
function callBackFromSubscribe(data){

  // log.info("fff1 on notify Callback")
  // log.info( data[0].toString())
  if(data.length > 0){

      var cmd = data[0].toString()
      updateQueriesIngoing(data);
      responsesFromRobot[cmd] = data
  }
}
