
var express = require('express')
var app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // console.log('in headers ', res)
  next();
});

app.get('/kill', function (req, res, next) {
  process.exit()
})

app.listen(5678, function () {
//   log.info('BLE server loaded successfully!')
})

setTimeout(function () {
    process.exit()

}, 500);