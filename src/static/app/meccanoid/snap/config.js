// let CLWebIntegrations = require('./node_modules/cl_react/modules/cl-web-integrations/')
let platform = 'reactjs'
let CLReact = require('./node_modules/cl-react/index');
// let mixpanel = require('mixpanel-browser');
// let ReactGA = require('react-ga');

var Config = {


  APP_NAME: 'Generalserver',

  PLATFORM: platform,

  /**
   * Defined the available app environments
   * @type {Object}
   */
  AVAILABLE_ENVIRONMENTS: {
    'localhost': 'mesapi.phantom2.com',
    'production': 'mesapi.phantom2.com',
    'charles': 'localhost.charlesproxy.com'
  },

  /**
   * Define the selected environment
   * e.g.'localhost' | 'development' | 'production'
   * @type {String}
   */
  SELECTED_ENV: 'production',

  /**
   * Set to 'true' if you want to cache the cl-api responses
   * @type {Boolean}
   */
  CACHE_API_RESPONSES: true,

  /**
   * Set to 'true' if you want to use SSL/HTTPS
   * @type {Boolean}
   */
  USE_HTTPS: false,

  /**
   * Set production log level
   *  'none' - don't show logs in production
   *  'low' - show only errors and warnings
   *  'high' - show all log levels
   * @type {String}
   */

  PRODUCTION_LOG_LEVEL: 'high',

  SEGMENTIO: {
    key: "vqnef7y8sm"
  },
  INTEGRATIONS: new CLReact.CLIntegrations(
    {
      platform:platform,
      vendors:{
        GOOGLE:{
          key:false,
          driver:false,
        },
        MIXPANEL:{
          key:false,
          driver:false,
        }
      }
    }),
}


// logs:


if (Config.SELECTED_ENV === 'production') {

  switch (Config.PRODUCTION_LOG_LEVEL) {

    case 'none':
      console.debug =
      console.info =
      console.log =
      console.warn =
      console.error = () => {};
      break;

    case 'low':
      console.debug =
      console.info =
      console.log = () => {};
      break;

    case 'high':
      break;

    default:
      break;
  }

}

//console.log("in conf",Config);

module.exports = Config;
