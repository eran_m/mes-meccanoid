// ziv
// FOR MAX ROBOT!!
// here we interact with a robot / animation / both
//

var lastMotorsBuffer = clone(RobotConstants.SetMotorsCommand);
var isWalking = false
// var isUpdateServos = false

// RobotControl methods are the API to the physical robot. 
// 
var RobotControl = {};

// local refs of widely-used constants
var RobotDirections = RobotConstants.RobotDirections;
// var ServoColors = RobotConstants.ServoColors;
// var ServoLocations = RobotConstants.ServoLocations;
var MotorLocations = RobotConstants.MotorLocations;
// var PCBLedNames = RobotConstants.PCBLedNames;



RobotControl.saviourPose = function() {

    // if servo is released - call Engage first and then call setServo !
    // var aServerIsReleased = false;
    // for (var i = 1; i <= 2; i++) {
    //     if (RobotState.servoStatus[i] === ServoReleaseMode.released) {
    //         aServerIsReleased = true;
    //     }
    // }
    // if (aServerIsReleased && (RobotState.handShakeStatus == 'handshaked')) {

    //     console.log("At last one servo is not engaged! trying to engage all now");
    //     this.setReleaseServoAll('engaged');
    //     callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, function () {
    //         setTimeout(
    //             function () {
    //                 internalSaviourPose(this);
    //             }, 244);
    //     }, false);

    // } else {

        internalSaviourPose(this);

    // }
}

function onIRResult(response){
    
    if (!response.status || !response.data) {
        return
    }
    RobotState.IRState.left = response.data.data[2];
    RobotState.IRState.right = response.data.data[1];

}


RobotControl.getIRStatus = function(whichSensor) {
    
    return RobotState.IRState[whichSensor];
}


RobotControl.walk = function(feet, direction, speed) {
    var data = RobotConstants.SetMotorsCommand;
    // snap dropdown adds slashes :(
    direction = direction.trim()
    speed = speed.trim()
    var dirCommand = RobotDirections[direction]
    var speedCommand = getSpeed(direction, speed)

    // movementAnimation(feet, direction, speed)

    if (feet == 'both' || feet == 'right') {
        data[MotorLocations.right.direction] = dirCommand
        data[MotorLocations.right.speed] = speedCommand

    }
    if (feet == 'both' || feet == 'left') {
        data[MotorLocations.left.direction] = dirCommand
        data[MotorLocations.left.speed] = speedCommand
    }
    RobotState.motors = data
    isWalking = true

}

// RobotControl.walk = function(feet, direction, speed) {
//     var data = RobotConstants.SmartMotorsSpeedCommand;
//     // snap dropdown adds slashes :(
//     direction = direction.trim()
//     speed = direction == 'ST' ? 'none' : speed.trim()
    
//     if (feet == 'both' || feet == 'right') {
//         data[1] = RobotConstants.SmartMotorSpeeds['right'][direction][speed]

//     }
//     if (feet == 'both' || feet == 'left') {
//         data[2] = RobotConstants.SmartMotorSpeeds['left'][direction][speed]
//     }
//     RobotState.motors = data;
//     isWalking = true

// }

// Need to check if this is working in spider like in meccanoid...

RobotControl.getPCBLed = function(ledName) {
    return RobotState.PCBStatus[PCBLedNames[ledName]] === 1;
}


RobotControl.setPCBLed = function(ledName, state) {

    var lightOn = state ? 1 : 0
    RobotState.PCBStatus[PCBLedNames[ledName]] = lightOn

    callRobot(RobotConstants.WRITE_API, RobotState.PCBStatus, false, false)
}

// Turn all on/off
RobotControl.setPCBLeds = function(state) {

    var lightOn = state ? 1 : 0
    var pcbLedsNames = Object.keys(PCBLedNames);
    for (var i = 0; i < pcbLedsNames.length; i++) {
        var ledName = pcbLedsNames[i];
        RobotState.PCBStatus[PCBLedNames[ledName]] = lightOn;
    }
    callRobot(RobotConstants.WRITE_API, RobotState.PCBStatus, false, false)
}

RobotControl.setRobotEyesColor = function(ledNumbers, colorName) {
    // ledNumber is zero based
    var rgb = RobotConstants.RgbColors[colorName];
    var bytes = rgbToTwoBytes(rgb);
    for (let i = 0; i < ledNumbers.length; i++) {
        let ledNumber = ledNumbers[i];
        RobotState.eyes[1 + (2*ledNumber)] = bytes.greenRedByte;
        RobotState.eyes[2 + (2*ledNumber)] = bytes.timeBlueByte;
    }
    // robot.setEyesColor(rgb.r, rgb.g, rgb.b);
    callRobot(RobotConstants.WRITE_API, RobotState.eyes, false, false)
}

// RobotControl.forceServoDegrees = function() {

//     var gotAnswers = false
//     callRobot(RobotConstants.WRITE_WITH_RESPONSE_FORCE_API, RobotConstants.CheckServosCommand, function(response){
//         onServosResult(response)
       
//     }, function(error){
//        return "N/A BAD RESPONSE";
//     })

//     return;
// }


// RobotControl.getServoPositionDegrees = function(servoName) {

//     // where to look for degree
//     var locationInBuffer = ServoLocations[RobotState.robotType][servoName]
//     // the dergree that was saved on last change
//     var degFromRobot = RobotState.servoPositions[locationInBuffer]
//     // convert from robot-value to gui-degrees angle
//     var degForUserGui = transformServoAngleToDegrees(servoName, degFromRobot)
//     ////console.log(servoName + ' robot: ' + degFromRobot + " -> angle: " + degForUserGui);
//     return degForUserGui;
// }

// RobotControl.getServoPositionRawValue = function(servoName) {

//     // where to look for degree
//     var locationInBuffer = ServoLocations[RobotState.robotType][servoName];
//     // the dergree that was saved on last change
//     var degFromRobot = RobotState.servoPositions[locationInBuffer];
//     return degFromRobot;
// }


// RobotControl.setReleaseServoAll = function(mode) {

//     for (var i = RobotState.servoStatus.length - 1; i >= 1; i--) {
//         var servoMode = (mode == 'release' ? ServoReleaseMode.released : ServoReleaseMode.engaged);
//         RobotState.servoStatus[i] = servoMode
//     }
//     callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, false, false)
// }


RobotControl.demoLEDAnimation = function () {
    var i = 0;
    setInterval(function(){

        i = i < 53 ? (i+1) : 1;
        RobotControl.playLEDAnimation(i);
    },2500);
}

RobotControl.playLEDAnimation = function (animationNumber) {
    RobotControl.playPreset(0x1D, animationNumber);    
    // RobotControl.playPreset(animationNumber, 0); // CHECKING    

}

RobotControl.playSoundEffect = function (soundNumber) {
    var cmd = clone(GeneralCommand);
    cmd[0] = RobotConstants.MainCommands.PlaySound;
    cmd[1] = (soundNumber >> 8); // upper byte
    cmd[2] = (soundNumber & 0xFF); // lower byte
    callRobot(RobotConstants.WRITE_API, cmd, false, false);
}

RobotControl.playPreset = function (presetCode, presetParam) {

    var cmd = clone(GeneralCommand);
    cmd[0] = RobotConstants.MainCommands.MB_PlayPreset;
    cmd[1] = presetCode;
    cmd[2] = presetParam ? presetParam : 0;

    callRobot(RobotConstants.WRITE_API, cmd, false, false)
}

RobotControl.playLim = function(limNumber) {
    var cmd = clone(GeneralCommand)
    cmd[0] = RobotConstants.MainCommands.MB_PlayLIM
    cmd[1] = limNumber
    callRobot(RobotConstants.WRITE_API, cmd, false, false)
}

// RobotControl.setReleaseServo = function(servoName, mode) {

//     var servoMode = (mode == 'release' ? ServoReleaseMode.released : ServoReleaseMode.engaged);
//     RobotState.servoStatus[ServoLocations[RobotState.robotType][servoName]] = servoMode;
//     callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, false, false)
// }

// // update and return current servo light status
// var getServoLightCommandArr = function (servo, color){

//     for(var i = 1; i < 16; i++){
//         // if it's the light we want to change, update proper place in state array. otherwise keep state same
//         RobotState.servoLights[i] = (i == servo ? color : RobotState.servoLights[i])
        
//     }
//     return RobotState.servoLights
// }


// function internalSetServoPositions(servoName, degree) {
//     // 3D
//     callAnimation(servoName, degree);
//     //
//     // Real robot
//     // servo get 'angle' values of 0..225
//     var robotDegree = transformDegreeToServoAngle(servoName, degree);
//     //console.log(servoName + ' angle: ' + degree + " -> robot: " + robotDegree);

//     var slot = ServoLocations[RobotState.robotType][servoName];
//     RobotState.servoPositions[slot] = robotDegree

//     var data = RobotState.servoPositions;
//     callRobot(RobotConstants.WRITE_API, data, false, false)
// }

function callAnimation(servoName, d) {

    // var d =( degree + SERVO_DEGREE_OFFSET ) / SERVO_DEGREE_RATIO

    switch (servoName) {

        case "rightArm":
            robot.moveRArm(d)
            break
        case "leftArm":
            robot.moveLArm(d)
            break
        case "rightElbow":
            robot.moveRElbow(d)
            break
        case "leftElbow":
            robot.moveLElbow(d)
            break
        case "rightShoulder":
            robot.moveRShoulder(d)
            break
        case "leftShoulder":
            robot.moveLShoulder(d)
            break
        case "neck":
            robot.moveNeck(d)
            break
        case "head":
            robot.moveHead(d)
            break
        case "leftArm":
            robot.moveHead(d)
            break
    }
}
// TODO : IMPLEMENT IF/WHEN WE DEAL WITH 3D FOR MAX!
// send correct move command to 3d bot
function updateMovementAnimation() {

    // if (!robot3dLoaded) {
    //     return
    // }

    // var rightDirection = RobotState.motors[MotorLocations.right.direction]
    // var leftDirection = RobotState.motors[MotorLocations.left.direction]
    // var rightSpeed = RobotState.motors[MotorLocations.right.speed]
    // var leftSpeed = RobotState.motors[MotorLocations.left.speed]
    // var higherSpeed = rightSpeed > leftSpeed ? rightSpeed : leftSpeed

    // //console.log("rightDirection " + rightDirection +", leftDirection: "+leftDirection)
    // // moving straight ahead/backwards
    // if (rightDirection == leftDirection && rightSpeed == leftSpeed) {

    //     if (rightDirection != RobotDirections.ST) {

    //         robot.moveLegs(rightSpeed, rightDirection)
    //     } else {

    //         //console.log("should stop rotate")
    //         robot.stopMovement()
    //     }
    // // moving to side
    // } else if (rightDirection != leftDirection) {
    //     //console.log("should rotate - legs direction not same")
    //     if (rightDirection == RobotDirections.FD) {
    //         //console.log("should rotateLeft, right moves forward")

    //         robot.rotateLeft(higherSpeed)
    //     } else if (rightDirection == RobotDirections.BK) {
    //         //console.log("should rotateRight, right moves back")

    //         robot.rotateRight(higherSpeed)
    //     } else if (leftDirection == RobotDirections.FD) {
    //         //console.log("should rotateRight, left moves forward")

    //         robot.rotateRight(higherSpeed)
    //     } else if (leftDirection == RobotDirections.BK) {
    //         //console.log("should rotateLeft, left moves back")

    //         robot.rotateLeft(higherSpeed)
    //     }

    // } else if (rightDirection == leftDirection && rightSpeed != leftSpeed) {
    //     //console.log("should rotate - legs speed not same")

    //     if (rightSpeed > leftSpeed) {

    //         robot.rotateLeft(higherSpeed)
    //     } else {

    //         robot.rotateRight(higherSpeed)
    //     }
    // }
}


function rgbToTwoBytes(rgb) {
    var time = 0 // not in use so always 0 .. if used shpuld be 0 - 7
    var greenRedByte = ((rgb.g >> 5) << 3) | (rgb.r >> 5)
    var timeBlueByte = time << 3 | (rgb.b >> 5)
    return {'greenRedByte': greenRedByte, 'timeBlueByte': timeBlueByte}
}


function getSpeed(direction, speed) {

    if (direction == 'ST') {
        return 0
    }
    return RobotConstants.RobotSpeeds[speed]
}


function internalSaviourPose (controller) {
    
    
    for(var i = 1; i<=5; i++ ){
        controller.setRobotEyesColor(i,'off');
    }
    controller.walk('both', 'ST', 'fast');
    controller.playPreset(0x0A, 0x19);
    // robot.resetPosition()

}




function onButtonsResult(byte) {

    var newButtonState = {
        blueButton: (byte & 0x01) != 0,
        redButton: (byte & 0x02) != 0,
        greenButton: (byte & 0x04) != 0,
        yellowButton: (byte & 0x08) != 0
    }

    var changed = false
    Object.keys(newButtonState).forEach(function (k, i) {
        if (newButtonState[k] != RobotState.buttonState[k]) {

            changed = true
            console.log(k + ( newButtonState[k] ? ' was pressed' : 'was released' ))
        }
    })

    if (changed) {

        RobotState.buttonState = newButtonState
    }

}

function getButtonState() {

    return RobotState.buttonState
}

function motorChanged() {

    var changed = false
    for (var i = lastMotorsBuffer.length - 1; i >= 0; i--) {
        if (lastMotorsBuffer[i] != RobotState.motors[i]) {
            changed = true
        }
    }
    return changed
}
function motorRunning(){
  for (var i = 1; i < RobotState.motors.length; i++) {

    if(RobotState.motors[i] != 0){
      return true
    }
  }
  return false
}

// continously send motor commands to bot
//
// since snap wait is at least 1 second, any motor command blocks running less then 1s apart will
// override\add to each other
// this allows user to set each leg motor with a different block for each leg, and both changes
// will take place simultanously
// loop is 10ms to start quickly, but will actually write only once in 1s

setInterval(function () {

    var elapsedMS = Date.now() - lastMotorChange
    if (elapsedMS < RobotConstants.MOTOR_DELAY) {
        return
    }

    updateMovementAnimation()

    if (RobotState.handShakeStatus == 'handshaked' && isWalking) {

        lastMotorsBuffer = clone(RobotState.motors)
        // callRobot(ALIVE_API, [],  false, false)
        callRobot(RobotConstants.WRITE_API, RobotState.motors, false, false)
        lastMotorChange = Date.now()
        isWalking = motorRunning()
    }

}, 10) // TODO CHANGE TO CONST

// // check servos positions
// setInterval(function () {

//     // read info from robot   
//     if (RobotState.handShakeStatus == 'handshaked') {

//         callRobot(RobotConstants.WRITE_WITH_RESPONSE_WEAK_API, RobotConstants.CheckServosCommand, onServosResult, false)
//     }
// }, RobotConstants.SERVOS_POLL_INTERVAL); 


// check IR status
setInterval(function () {

    // read info from robot   
    if (RobotState.handShakeStatus == 'handshaked') {

        callRobot(RobotConstants.WRITE_WITH_RESPONSE_WEAK_API, RobotConstants.IRReadCommand, onIRResult, false)
    }
}, RobotConstants.IR_POLL_INTERVAL); 