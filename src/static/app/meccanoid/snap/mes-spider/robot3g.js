// RobotStageMorph /////////////////////////////////////////////////////////

RobotStageMorph.prototype = new StageMorph();
RobotStageMorph.prototype.constructor = RobotStageMorph;
RobotStageMorph.prototype.dimensions = new Point(480, 300); // unscaled extent
RobotStageMorph.prototype.frameRate = 0; // unscheduled per default
RobotStageMorph.prototype.setScale = StageMorph.prototype.setScale;


var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
var clock = new THREE.Clock();
var renderer, mesh, grid, material;

var robotStageInitialized = false;

var speeds = RobotConstants.RobotSpeeds
// var light = new THREE.DirectionalLight( 0x707070 );
// light.position.set( 0, 1, 1 ).normalize();
// scene.add(light);

var light = new THREE.DirectionalLight( 0x101010 );
light.position.set( 0, 1, 1 ).normalize();
var lightL = new THREE.DirectionalLight( 0x101010 );
lightL.position.set( -1, 1, 1 ).normalize();
var lightR = new THREE.DirectionalLight( 0x101010 );
lightR.position.set( 1, 1, 1 ).normalize();

scene.add(light);
scene.add(lightL);
scene.add(lightR);

function init(object) {
    scene.add(object)
    var render = function () {

        if(typeof(renderer) !== 'undefined'){

            renderer.render(scene, camera);
            setTimeout(function() {
                requestAnimationFrame( render );
            },100);
        }else{
            // console.log("renderer was undefined!!!")
             setTimeout(function() {
                renderer.render(scene, camera);
                setTimeout(function() {
                    requestAnimationFrame( render );
                },100);
            },3);
        }
        

  };
  render();
}

var REFRESH_RATE = 10;

var SLOW_STEP = 0.2;
var MEDIUM_STEP = 0.3;
var FAST_STEP = 0.4;

var SLOW_DEGREE = 360 / 9.5;
var MEDIUM_DEGREE = 360 / 7;
var FAST_DEGREE = 360 / 4.5;

var shouldMoveForward = false;
var shouldMoveBackward = false;
var shouldRotateLeft = false;
var shouldRotateRight = false;
var robotStep = SLOW_STEP; // default
var robotStepBack = SLOW_STEP; // default
var leftRotationDegree = SLOW_DEGREE; // default
var rightRotationDegree = SLOW_DEGREE; // default

var lightsOn = false;

var loader = new THREE.JSONLoader();
loader.crossOrigin = ""
// load a resource
loader.load('http://localhost:5678/snap/mes-spider/robot-rigged.json',

  function ( geometry, materials ) {
    // alert("loaded JSON")
    for (var i = 0; i < materials.length; i++) {
      materials[i].skinning = true
    }
    material = new THREE.MultiMaterial( materials );
    mesh = new THREE.SkinnedMesh(geometry, material)

    // var skeletonHelper = new THREE.SkeletonHelper( mesh );
    // skeletonHelper.material.linewidth = 10;
    // scene.add( skeletonHelper );

    for (bone in bonesMapping) {
        robotBones[bone] = findBoneByName(bonesMapping[bone]);
    }

    ROBOT_LEGS = robotBones['Hips'];
    ROBOT_LEGS.position.x = 0;
    ROBOT_LEGS.position.y = 0;
    ROBOT_LEGS.position.z = 0;

    init(mesh);

    camera.position.x = CAMERA_DISTANCE * Math.sin( Math.radians(cameraDegrees) );
    camera.position.z = CAMERA_DISTANCE * Math.cos( Math.radians(cameraDegrees) );
    camera.lookAt( mesh.position );

    // start legs movement/rotation timer
    setInterval(function () {
      if (shouldMoveForward) {
        grid.position.x -= robotStep * Math.sin( ROBOT_LEGS.rotation.y) / REFRESH_RATE;
        grid.position.z -= robotStep * Math.cos( ROBOT_LEGS.rotation.y) / REFRESH_RATE;
      }
      if (shouldMoveBackward) {
        grid.position.x += robotStepBack * Math.sin( ROBOT_LEGS.rotation.y) / REFRESH_RATE;
        grid.position.z += robotStepBack * Math.cos( ROBOT_LEGS.rotation.y) / REFRESH_RATE;
      }
      if (shouldRotateLeft) {
        var rotationDegree = leftRotationDegree;
        if (shouldRotateRight && rightRotationDegree > leftRotationDegree) {
          rotationDegree = rightRotationDegree;
        }
        ROBOT_LEGS.rotation.y += Math.radians(rotationDegree) / REFRESH_RATE;
      }
      if (shouldRotateRight) {
        var rotationDegree = rightRotationDegree;
        if (shouldRotateLeft && leftRotationDegree > rightRotationDegree) {
          rotationDegree = leftRotationDegree;
        }
        ROBOT_LEGS.rotation.y -= Math.radians(rightRotationDegree) / REFRESH_RATE;
      }
    }, 1000 / REFRESH_RATE);

    //scene.add(mesh);

    //renderer.render(scene, camera);

  }
);

var CAMERA_DISTANCE = 9;

cameraDegrees = 0;

camera.position.y = 3;
camera.position.z = CAMERA_DISTANCE;
camera.position.x = 0;

var bonesMapping = {
    'Hips' : 'HIPS',
    'Neck' : 'neck',
    'Head' : 'head',
    'RShoulder' : 'shoulder_r',
    'LShoulder' : 'shoulder_l',
    'RArm' : 'arm_r',
    'LArm' : 'arm_l',
    'RElbow' : 'forearm_r',
    'LElbow' : 'forearm_l'
}

var robotBones = {};
var ROBOT_LEGS;

var isFrameStepping = false;
var timeToStep = 0;

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

function findBoneByName(name, bone) {
    if (!bone) {
        return findBoneByName(name, mesh.children[0]);
    } else {
        if (bone.name == name) {
            return bone;
        }
        if (bone.children.length > 0) {
            for (var i=0;i<bone.children.length;i++) {
                var b = findBoneByName(name, bone.children[i]);
                if (b) {
                    return b;
                }
            }
        }
    }

}
function RobotStageMorph(globals, force) {
    this.init(globals, force);
}

RobotStageMorph.prototype.init = function (globals, force) {

    if (!robotStageInitialized) {

        console.log('initializing robot stage...')


        this.setColor(new Color(220, 220, 220));
        var canvas = document.getElementById('world3g');

       var ctx = canvas.getContext("2d");

        var background = new Image();
        background.src = "mes-spider/spider_small.png";

        // Make sure the image is loaded first otherwise nothing will draw.
        background.onload = function(){
            ctx.drawImage(background,35,0);   
        }
        // var ctx = canvas.getContext('webgl');
        // renderer = new THREE.WebGLRenderer({canvas : canvas, context: ctx});
        // renderer.setSize( window.innerWidth, window.innerHeight );

        // grid = new THREE.GridHelper( 100, 200, 0x808080, 0x808080 );
        // grid.position.set( 0, - 0.04, 0 );
        // scene.add( grid );


        // document.body.appendChild( renderer.domElement );

        robotStageInitialized = true;

    } else {

      console.log('robot stage already initiailzed')
    }

}

// RobotMorph /////////////////////////////////////////////////////////

RobotMorph.prototype = new Morph();
RobotMorph.prototype.constructor = RobotMorph;
RobotMorph.prototype.paletteColor = new Color(55, 55, 55);
RobotMorph.prototype.thumbnail = SpriteMorph.prototype.thumbnail;
RobotMorph.prototype.blockForSelector = SpriteMorph.prototype.blockForSelector;
RobotMorph.prototype.freshPalette = SpriteMorph.prototype.freshPalette;
RobotMorph.prototype.blockColor = SpriteMorph.prototype.blockColor;

function RobotMorph(globals) {
    this.init(globals);
}

RobotMorph.prototype.init = function (globals) {
    this.name = localize('Robot');
    this.blocksCache = {};
    this.customBlocks = [];
    this.scripts = new ScriptsMorph(this);
};

// RobotMorph.prototype.initBlocks = function () {
//     RobotMorph.prototype.blocks = {

//         // Motion
//         moveHead: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move head %n degrees',
//             defaults: [10]
//         },
//         moveNeck: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move neck %n degrees',
//             defaults: [10]
//         },
//         moveLShoulder: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move left shoulder %n degrees',
//             defaults: [10]
//         },
//         moveRShoulder: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move right shoulder %n degrees',
//             defaults: [10]
//         },
//         moveLArm: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move left arm %n degrees',
//             defaults: [10]
//         },
//         moveRArm: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move right arm %n degrees',
//             defaults: [10]
//         },
//         moveLElbow: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move left elbow %n degrees',
//             defaults: [10]
//         },
//         moveRElbow: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move right elbow %n degrees',
//             defaults: [10]
//         },
//         rotateLeft: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'rotate left %n degrees',
//             defaults: [10]
//         },
//         rotateRight: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'rotate right %n degrees',
//             defaults: [10]
//         },
//         moveLegs: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move forward at %s speed',
//             defaults: ['slow']
//         },
//         moveLegsBackward: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'move backward at %s speed',
//             defaults: ['slow']
//         },
//         stopMovement: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'stop movement'
//         },
//         rotateCameraLeft: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'rotate camera left',
//             defaults: [10]
//         },
//         rotateCameraRight: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'rotate camera right',
//             defaults: [10]
//         },
//         turnOnEyes: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'turn on eyes lights'
//         },
//         turnOffEyes: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'turn off eyes lights'
//         },
//         setEyesColor: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'set eyes color',
//             defaults: [0, 255, 0]
//         },
//         resetPosition: {
//             only: RobotMorph,
//             type: 'command',
//             category: 'motion',
//             spec: 'reset robot position',
//         },
//     }
// }

// RobotMorph.prototype.initBlocks();

RobotMorph.prototype.initBlockMigrations = function () {
    // RobotMorph.prototype.blockMigrations = {
    //     doStopAll: {
    //         selector: 'doStopThis',
    //         inputs: [['all']]
    //     },
    //     doStop: {
    //         selector: 'doStopThis',
    //         inputs: [['this script']]
    //     },
    //     doStopBlock: {
    //         selector: 'doStopThis',
    //         inputs: [['this block']]
    //     },
    //     receiveClick: {
    //         selector: 'receiveInteraction',
    //         inputs: [['clicked']]
    //     },
    //     reportTrue: {
    //         selector: 'reportBoolean',
    //         inputs: [true]
    //     },
    //     reportFalse: {
    //         selector: 'reportBoolean',
    //         inputs: [false]
    //     }
    // };
};
RobotMorph.prototype.initBlockMigrations();

RobotMorph.prototype.blockTemplates = function (category) {
    var blocks = [];

    function block(selector) {
        var newBlock = RobotMorph.prototype.blockForSelector(selector, true);
        newBlock.isTemplate = true;
        return newBlock;
    }

    blocks.push(block('moveHead'));
    blocks.push(block('moveNeck'));
    blocks.push(block('moveLShoulder'));
    blocks.push(block('moveRShoulder'));
    blocks.push(block('moveLArm'));
    blocks.push(block('moveRArm'));
    blocks.push(block('moveLElbow'));
    blocks.push(block('moveRElbow'));
    blocks.push(block('rotateLeft'));
    blocks.push(block('rotateRight'));
    blocks.push(block('moveLegs'));
    blocks.push(block('moveLegsBackward'));
    blocks.push(block('stopMovement'));
    blocks.push(block('rotateCameraLeft'));
    blocks.push(block('rotateCameraRight'));
    blocks.push(block('turnOnEyes'));
    blocks.push(block('turnOffEyes'));
    blocks.push(block('setEyesColor'));
    blocks.push(block('resetPosition'));

    return blocks;
}

RobotMorph.prototype.saviourMode = function() {

    console.log("saviour mode called!")
}

RobotMorph.prototype.drawNew = function (globals) {

    this.robotStage = this.parentThatIsA(RobotStageMorph);
    this.image = newCanvas(this.extent());

}

RobotMorph.prototype.redrawSkelton = function() {

        // TODO ACTIVATE AFTER WE HAVE MESH
        // renderer.render( scene, camera );

}

RobotMorph.prototype.moveHead = function(degrees) {
    var bone = robotBones['Head'];
    if (bone) {
        bone.rotation.z = Math.radians(degrees * 0.3);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveNeck = function(degrees) {
    var bone = robotBones['Neck'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveLShoulder = function(degrees) {
    var bone = robotBones['LShoulder'];
    if (bone) {
        bone.rotation.x = Math.radians(degrees) ;
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveRShoulder = function(degrees) {
    var bone = robotBones['RShoulder'];
    if (bone) {
        bone.rotation.x = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveLArm = function(degrees) {
    var bone = robotBones['LArm'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees * -1);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveRArm = function(degrees) {
    var bone = robotBones['RArm'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveLElbow = function(degrees) {
    var bone = robotBones['LElbow'];
    if (bone) {
        bone.rotation.z = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveRElbow = function(degrees) {
    var bone = robotBones['RElbow'];
    if (bone) {
        bone.rotation.z = Math.radians(degrees * -1);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.rotateLeft = function(speed) {
  shouldRotateRight = false;
  if (speed == speeds.slow) {
    leftRotationDegree = SLOW_DEGREE;
  } else if (speed == speeds.medium) {
    leftRotationDegree = MEDIUM_DEGREE;
  } else if (speed == speeds.fast) {
    leftRotationDegree = FAST_DEGREE;
  }
//  console.log("rotate left called")
  shouldRotateLeft = true;
}

RobotMorph.prototype.rotateRight = function(speed) {
  shouldRotateLeft = false;
  if (speed == speeds.slow) {
    rightRotationDegree = SLOW_DEGREE;
  } else if (speed == speeds.medium) {
    rightRotationDegree = MEDIUM_DEGREE;
  } else if (speed == speeds.fast) {
    rightRotationDegree = FAST_DEGREE;
  }
//  console.log("rotate right called")

  shouldRotateRight = true;
}

RobotMorph.prototype.moveLegs = function(speed, direction) {
  if (direction == RobotConstants.RobotDirections.FD) {
      if (speed == speeds.slow) {
        robotStep = SLOW_STEP;
      } else if (speed == speeds.medium) {
        robotStep = MEDIUM_STEP;
      } else if (speed == speeds.fast) {
        robotStep = FAST_STEP;
      }
  } else {
      if (speed == speeds.slow) {
        robotStepBack = SLOW_STEP;
      } else if (speed == speeds.medium) {
        robotStepBack = MEDIUM_STEP;
      } else if (speed == speeds.fast) {
        robotStepBack = FAST_STEP;
      }
  }
  // stopping sidewise movement when both legs should go FD
  shouldRotateRight = false
  shouldRotateLeft = false

// TO DO IMPLEMENT If/WHEN WE GET 3D ANIMATION FOR MAX ROBOT
  // shouldMoveBackward = direction == RobotConstants.RobotDirections.BK
  // shouldMoveForward = direction == RobotConstants.RobotDirections.FD
}

// RobotMorph.prototype.moveLegsBackward = function(speed) {
//   if (speed == 'slow') {
//     robotStepBack = SLOW_STEP;
//   } else if (speed == 'medium') {
//     robotStepBack = MEDIUM_STEP;
//   } else if (speed == 'fast') {
//     robotStepBack = FAST_STEP;
//   }

//   // stopping sidewise movement when both legs should go BK
//   shouldRotateRight = false
//   shouldRotateLeft = false

// }

RobotMorph.prototype.stopMovement = function() {
  shouldMoveForward = false;
  shouldMoveBackward = false;
  shouldRotateLeft = false;
  shouldRotateRight = false;
}

RobotMorph.prototype.rotateCameraLeft = function() {
    cameraDegrees -= 10;
    camera.position.x = CAMERA_DISTANCE * Math.sin( Math.radians(cameraDegrees) );
    camera.position.z = CAMERA_DISTANCE * Math.cos( Math.radians(cameraDegrees) );
    camera.lookAt( mesh.position );
}

RobotMorph.prototype.rotateCameraRight = function() {
    cameraDegrees += 10;
    camera.position.x = CAMERA_DISTANCE * Math.sin( Math.radians(cameraDegrees) );
    camera.position.z = CAMERA_DISTANCE * Math.cos( Math.radians(cameraDegrees) );
    camera.lookAt( mesh.position );
}

RobotMorph.prototype.turnOnEyes = function() {
    if (!lightsOn) {
      material.materials[0].color.addScalar(1);
      lightsOn = true;
    }
}

RobotMorph.prototype.turnOffEyes = function() {
    if (lightsOn) {
      material.materials[0].color.addScalar(-1);
      lightsOn = false;
    }
}

RobotMorph.prototype.setEyesColor = function(r, g, b) {
    material.materials[0].color.setRGB(r, g, b);
}

RobotMorph.prototype.resetPosition = function() {

    // shouldMoveForward = false;
    // shouldMoveBackward = false;
    // shouldRotateLeft = false;
    // shouldRotateRight = false;

    // ROBOT_LEGS.position.x = 0;
    // ROBOT_LEGS.position.y = 0;
    // ROBOT_LEGS.position.z = 0;
    // ROBOT_LEGS.rotation.y = 0;

    // cameraDegrees = 0;
    // camera.position.x = CAMERA_DISTANCE * Math.sin( Math.radians(cameraDegrees) );
    // camera.position.z = CAMERA_DISTANCE * Math.cos( Math.radians(cameraDegrees) );
    // camera.lookAt( mesh.position );
}

setTimeout(function(){
robot3dLoaded = true;

},1500)
