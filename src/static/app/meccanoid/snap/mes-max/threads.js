
// meccanoid wait for servo degree reading to refresh then report value
Process.prototype.reportServoDegreeGreaterThan = function(servoName, degreeToCheck) {
    return this.readServo(servoName, false) > degreeToCheck;
}

Process.prototype.reportServoDegreeLessThan = function(servoName, degreeToCheck) {
    return this.readServo(servoName,  false) < degreeToCheck;
}

// Process.prototype.doWaitUntil = function (goalCondition) {
//     if (goalCondition) {
//         this.popContext();
//         this.pushContext('doYield');
//         return null;
//     }
//     this.context.inputs = [];
//     this.pushContext('doYield');
//     this.pushContext();
// };
var lastQTime = Date.now();
Process.prototype.readServo = function(servoName, noisy) {
    // determine if we want to show the result as a bubble in GUI
    var silently = typeof(noisy) != 'undefined' && !noisy;
    // even if "noisy", show bubble only if user directly clicked the block
    if (!silently){
        silently = (this.context.parentContext != null && !Process.prototype.enableSingleStepping); // user not clicked directly and stepping not visible
    }else{
        silently = !Process.prototype.enableSingleStepping; // override silent reporting if visible stepping enabled
    }

    if(RobotState.handShakeStatus != 'handshaked'){
        this.popContext();
        this.pushContext('doYield');
        // if robot disconnected - return last set angle value
        //var toReturnValue = 0;
        var toReturnValue = robotGetServoAngle(servoName);
        if (!silently){
            this.topBlock.showBubble(
                toReturnValue,
                this.exportResult
            );
        }
        return toReturnValue;
    }

    if (!RobotState.isCurrentlyPollingServos){
        lastQTime = Date.now()
        console.log("starting to poll servos")
        RobotState.isCurrentlyPollingServos = true;
        robotForceServoAngle(servoName);
    }

    if (robotForceServoAngleContinueWaiting()) {
        
        var lasted = Date.now() - lastQTime 
        // console.log("got result from bot, after: "+ (lasted /1000))
        
        this.popContext();
        this.pushContext('doYield');        
        if (!silently){        
            this.topBlock.showBubble(
                robotGetServoAngle(servoName),
                this.exportResult
            );
        }            
        RobotState.isCurrentlyPollingServos = false;
        return robotGetServoAngle(servoName);
    }
        // console.log("no resulto")

    this.context.inputs = [];
    this.pushContext('doYield');
    this.pushContext();
};