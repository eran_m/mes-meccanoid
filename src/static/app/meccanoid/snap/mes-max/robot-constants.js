
// values to use in first byte of command array
RobotConstants.MainCommands = {
    checkStatus: 1,
    handShake: 26,
    servoColor: 12,
    servoMove: 8,
    MB_GetServoPos: 9,
    MB_SetMotorValues: 13,
    MB_SetPCBLED: 28,
    MB_SetRGBLEDColor: 0x11,
    MB_PlayPreset: 0x19, // play animation
    setServoStatus: 11,
    getServoStatus: 10,
    MB_PlayLIM: 21,
    SmartMotorsCommand: 43,
    SmartMotorsSpeedCommand: 53,
    PlaySound: 0x30,
    MAX_IR_DISTANCE: 0x23,
}


// possible values for set color commands
RobotConstants.ServoColors = {
    off: 0,
    red: 1,
    green: 2,
    lime: 3,
    blue: 4,
    purple: 5,
    aqua: 6,
    white: 7
}

RobotConstants.RgbColors = {
    off: {r: 0, g: 0, b: 0},
    red: {r: 255, g: 0, b: 0},
    green: {r: 0, g: 255, b: 0},
    lime: {r: 0, g: 255, b: 255},
    blue: {r: 0, g: 0, b: 255},
    purple: {r: 255, g: 0, b: 255},
    aqua: {r: 255, g: 255, b: 0},
    white: {r: 255, g: 255, b: 255}
}

/*
// adressess for motors, servos, and lights (i.e places in byte array)
var servLocationNames = [
    "rightArm",
    "leftArm",
    "rightShoulder",
    "leftShoulder",
    "leftElbow",
    "rightElbow",
    "head",
    "neck"];
*/

RobotConstants.ServoLocations = {
    big: {
        hand: 2,
        head:1
    },
    small: {
        hand: 2,
        head:1
    }
}

var servoNameForLocation = function(location, robotType){
    return Object.keys(RobotConstants.ServoLocations[robotType]).find(key => RobotConstants.ServoLocations[robotType][key] === location);
}

// realAngle: human (gui) degrees, robotAngle: value actually read from robot
RobotConstants.ServoAngleToRealDegrees = {
    head: {realAngle: [-60, -30, 0, 30, 60], robotAngle: [57, 90, 136, 156, 189]},
    hand: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [24, 57, 90, 123, 156, 189, 222]},

    // neck: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [24, 57, 90, 133, 156, 189, 222]},
    // rightShoulder: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [222, 189, 156, 123, 90, 57, 24]},
    // rightArm: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [24, 57, 90, 123, 156, 189, 222]},
    // rightElbow: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [24, 57, 90, 133, 156, 189, 222]},
    // leftShoulder: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [24, 57, 90, 123, 156, 189, 222]},
    // leftArm: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [222, 189, 156, 133, 90, 57, 24]},
    // leftElbow: {realAngle: [-90, -60, -30, 0, 30, 60, 90], robotAngle: [222, 189, 156, 123, 90, 57, 24]}
}

RobotConstants.SmartMotorSpeeds = {
    left: {
        FD:{
            fast: 0x1A,
            medium: 0x14,
            slow: 0x11,
        },
        BK:{
            fast: 0x2A,
            medium: 0x24,
            slow: 0x21,
        },
        ST:{none:0x50}
    },
    right: {
        BK:{
            fast: 0x1A,
            medium: 0x14,
            slow: 0x11,
        },
        FD:{
            fast: 0x2A,
            medium: 0x24,
            slow: 0x21,
        },
        ST:{none:0x50}
    }
}


// RobotConstants.MotorLocations = {
//     right: {
//         direction: 2,
//         speed: 4
//     },
//     left: {
//         direction: 1,
//         speed: 3
//     }
// }

RobotConstants.PCBLedNames = {
    blue: 1,
    red: 2,
    green: 3,
    yellow: 4
}



// ready-made byte arrays for initializations / commands that don't have params
RobotConstants.InitialColors = [RobotConstants.MainCommands.servoColor, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.InitialServoPositions = [RobotConstants.MainCommands.servoMove, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

RobotConstants.HandshakeCommand = [RobotConstants.MainCommands.handShake, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.CheckStatusCommand = [RobotConstants.MainCommands.checkStatus, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.CheckServosCommand = [RobotConstants.MainCommands.MB_GetServoPos, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.SetMotorsCommand = [RobotConstants.MainCommands.MB_SetMotorValues, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.PCBLedsCommand = [RobotConstants.MainCommands.MB_SetPCBLED, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.EyesColorCommand = [RobotConstants.MainCommands.MB_SetRGBLEDColor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
RobotConstants.ServoStatusCommand = [RobotConstants.MainCommands.setServoStatus, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
RobotConstants.SmartMotorsCommand = [RobotConstants.MainCommands.SmartMotorsCommand, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0];
RobotConstants.SmartMotorsSpeedCommand = [RobotConstants.MainCommands.SmartMotorsSpeedCommand, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
RobotConstants.IRReadCommand = [RobotConstants.MainCommands.MAX_IR_DISTANCE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

// RobotConstants.ServoStatusCheckCommand = [RobotConstants.MainCommands.getServoStatus,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


RobotConstants.INITIAL_ROBOT_STATE = {
    name:         "",
    handShakeStatus: 'none',
    servoLights:  RobotConstants.InitialColors,
    servoPositions: RobotConstants.InitialServoPositions,
    servoStatus:  clone(RobotConstants.ServoStatusCommand),
    PCBStatus: RobotConstants.PCBLedsCommand,
    motors:  clone(RobotConstants.SmartMotorsSpeedCommand),
    robotType : 'big',
    buttonState: {
      blueButton : false,
      redButton : false,
      greenButton : false,
      yellowButton : false
    },
    lastServoStatus:-1,
    lastPollingTs:-1,
    isCurrentlyPollingServos:false,
    IRState:{
        left:14,
        right:14
    }
};

RobotConstants.SUPPORTED_ROBOT_NAME = "MAX";
RobotConstants.ROBOT_MODEL = "MAX";
