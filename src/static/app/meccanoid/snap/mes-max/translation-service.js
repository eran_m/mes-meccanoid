// var robotSetServoAngle = function (servo, angle) {
//     servo = snapToString(servo);
//     angle = snapToInt(angle);
//     RobotControl.setServoPositions(translateServoName(servo), translateAngle(angle));
// }

handPositions = {
    'open': 30,
    'hold wide': 55,
    'hold narrow': 75,
    'close': 90
}
var robotSetHandPosition = function (position) {

    position = snapToString(position);
    let angle = lookupValueInObject(handPositions, position);// position === 'open' ? -30 : 90;
    RobotControl.setServoPositions('hand', translateAngle(angle));
}

headDirections = {
    left: 45,
    center: 0,
    right: -45
};

var robotTiltHead = function (direction) {

    direction = snapToString(direction);
    let angle = lookupValueInObject(headDirections, direction);
    RobotControl.setServoPositions('head', translateAngle(angle));
}

var robotReadDistanceSensor = function (whichSensor) {
    whichSensor = snapToString(whichSensor);
    return translateIRtoDistance(RobotControl.getIRStatus(whichSensor));
}

var robotMotorStart = function (motor, direction, speed) {
    motor = snapToString(motor);
    direction = snapToString(direction);
    speed = snapToString(speed);
    RobotControl.walk(translateMotorName(motor), translateMotorDirection(direction), translateMotorSpeed(speed));
}
var robotMotorStop = function (motor) {
    motor = snapToString(motor);
    RobotControl.walk(translateMotorName(motor), 'ST', 'fast'); // last parameter is dontcare
}
var robotSetServoLight = function (servo, color) {
    servo = snapToString(servo);
    color = snapToString(color);
    RobotControl.setRobotLight(translateServoName(servo), translateServoColor(color));
}
var robotSetEyesColor = function (color) {
    color = snapToString(color);
    RobotControl.setRobotEyesColor(translateServoColor(color));
}
var robotSetBeltColor = function (beltButton, onOrOff) {
    beltButton = snapToString(beltButton);
    onOrOff = snapToString(onOrOff);
    RobotControl.setPCBLed(translateBeltLedName(beltButton), translateBeltLedOnOrOff(onOrOff))
}
var robotReleaseServo = function (servo) {
    servo = snapToString(servo);
    return RobotControl.setReleaseServo(translateServoName(servo), 'release');
}
var robotEngageServo = function (servo) {
    servo = snapToString(servo);
    return RobotControl.setReleaseServo(translateServoName(servo), 'engage');
}
var robotGetServoAngle = function (servo) {
    servo = snapToString(servo);
    return RobotControl.getServoPositionDegrees(translateServoName(servo));
}
var robotForceServoAngle = function (servo) {
    servo = snapToString(servo);
    var result = RobotControl.forceServoDegrees()
    RobotState.lastPollingTs = Date.now();
    return result;
}
var robotForceServoAngleContinueWaiting = function () {
    if (RobotState.handShakeStatus == 'handshaked') {
        return RobotState.lastPollingTs < RobotState.lastServoStatus;
    } else {
        return false;
    }
}
var robotIsConnected = function () {
    return (RobotState.handShakeStatus == 'handshaked');
}
var robotGetServoRawAngle = function (servo) {
    servo = snapToString(servo);
    return RobotControl.getServoPositionRawValue(translateServoName(servo));
}
var robotGetBeltButtonStatus = function (beltButton) {
    beltButton = snapToString(beltButton);
    return RobotControl.getPCBLed(translateBeltLedName(beltButton));
}
var robotToStartupState = function () {
    RobotControl.saviourPose();
}
var robotPlayMyRecording = function (recordingNumber) {
    recordingNumber = snapToInt(recordingNumber);
    if (recordingNumber < 1 || recordingNumber > 15) {
        throw new Error('Value must be between 1 and 15');
    }
    //var limCommandCode = lookupValueInObject(limCommandCodes, recordingNameStr);
    RobotControl.playLim(recordingNumber);
}
var robotPlayPredefinedRecording = function (predefinedRecordingStr) {
    predefinedRecordingStr = snapToString(predefinedRecordingStr);
    var commandCode = lookupValueInObject(predefinedPlayCommandsCodes, predefinedRecordingStr);
    RobotControl.playPreset(commandCode.a, commandCode.b);
}

var ledAnimations = {
    'none':0,
    'hello':28,
    'bye':17,
    '?':2,
    'happy':3,
    'laughing':3,
    'wink':13,
    'sad':1,
    'heart':4,
    'sun':6,
}
var robotShowAnimation = function (animationName) {
    animationName = snapToString(animationName);
    // allow parameter to be a number - then dont translate!!
    var commandCode = parseInt(animationName);
    if (isNaN(commandCode)) {
        commandCode = lookupValueInObject(ledAnimations, animationName);
    }
    RobotControl.playLEDAnimation(commandCode);
}

// Play SFX
var robotPlaySound = function (sfxNumber) {
    sfxNumber = snapToInt(sfxNumber);
    RobotControl.playSoundEffect(sfxNumber);
}

var englishMotorNames = {
    'right leg': 'right',
    'left leg': 'left',
    'both legs': 'both'
}
var englishMotorDirections = {
    'forward': 'FD',
    'backward': 'BK'
    // 'stop': 'ST'
}
var englishMotorSpeeds = {
    'fast': 'fast',
    'medium': 'medium',
    'slow': 'slow'
}
// left side is GUI
var englishServoColor = {
    'white': 'white',
    'yellow': 'aqua',
    'green': 'green', 
    'cyan': 'lime',
    'blue': 'blue',
    'purple': 'purple',
    'red': 'red',
    'off': 'off'
}
var englishBeltLeds = {
    'green': 'green',
    'red': 'red',
    'blue': 'blue',
    'yellow': 'yellow'
}
var englishBeltLedOfOrOff = {
    'on': true,
    'off': false
}
/*var limCommandCodes = {
    '#1' : 1,
    '#2' : 2,
    '#3' : 3,
    '#4' : 4,
    '#5' : 5,
    '#6' : 6,
    '#7' : 7,
    '#8' : 8,
    '#9' : 9,
    '#10' : 10,
    '#11' : 11,
    '#12' : 12,
    '#13' : 13,
    '#14' : 14,
    '#15' : 15
}*/
// Parameter #2 for BLE command #19 (Preset Commands)
var predefinedPlayCommandsCodes = {
    'two truths and a lie'              : { a: 0x01, b: 0x19},
    'are you smarter than a robot'      : { a: 0x02, b: 0x19},
    'go on patrol'                      : { a: 0x03, b: 0x19},
    'go to sleep'                       : { a: 0x04, b: 0x19},
    'i want to show you something'      : { a: 0x05, b: 0x19},
    'let me introduce you'              : { a: 0x06, b: 0x19},
    'do a dance'                        : { a: 0x07, b: 0x19},
    'dj max'                            : { a: 0x08, b: 0x19},
    'play a game'                       : { a: 0x08, b: 0x19},
    'make a delivery'                            : { a: 0x09, b: 0x19},
    'robo tennis'                      : { a: 0x0A, b: 0x19},
    'set a reminder'                     : { a: 0x0B, b: 0x19},
    'set clock'                        : { a: 0x0C, b: 0x19},
    'set volume'                 : { a: 0x0D, b: 0x19},
    'take a message'                : { a: 0x0E, b: 0x19},
    'tell me a fun fact'                    : { a: 0x0F, b: 0x19},
    'tell me a joke'                  : { a: 0x10, b: 0x19},
    'tell me my fortune'             : { a: 0x11, b: 0x19},
    'would you rather'               : { a: 0x12, b: 0x19},
    'initial startup'             : { a: 0x13, b: 0x19},
    'random thoughts'           : { a: 0x14, b: 0x19},
    'robo dreams'             : { a: 0x15, b: 0x19},
    'recall'                  : { a: 0x16, b: 0x19},
    'hot seat'           : { a: 0x17, b: 0x19},
    'suggest activities'               : { a: 0x18, b: 0x19},
    'idle'                    : { a: 0x0D, b: 0x19}
}

var englishServoNames = {
    'head': 'head',
    'hand': 'hand'
}

var translateServoName = function (servoTitle) {
    return lookupValueInObject(englishServoNames, servoTitle);
}
var translateAngle  = function (angle) {
    if (angle < -90 || angle > 90) {
        throw new Error('Angle must be between -90 and 90');
    }
    return angle
}
var translateMotorName = function (motorTitle) {
    return lookupValueInObject(englishMotorNames, motorTitle);
}
var translateMotorDirection = function (directionTitle) {
    return lookupValueInObject(englishMotorDirections, directionTitle);
}
var translateMotorSpeed = function (speedTitle) {
    return lookupValueInObject(englishMotorSpeeds, speedTitle);
}
var translateServoColor = function (colorTitle) {
    return lookupValueInObject(englishServoColor, colorTitle);
}
var translateBeltLedName = function (beltLedTitle) {
    return lookupValueInObject(englishBeltLeds, beltLedTitle);
}
var translateBeltLedOnOrOff = function (beltLedOnOrOffTitle) {
    return lookupValueInObject(englishBeltLedOfOrOff, beltLedOnOrOffTitle);
}
var translateIRtoDistance = function (rawIRValue) {
    // input is value between 0 (near) and 14 (far)
    // returns 1..13 or 'none' (1=near, 13=far)
    if (rawIRValue >= 14) {
        return 'none';
    } else {
        return rawIRValue + 1;
    }
}

var lookupValueInObject = function (anObject, aValue) {
    var result = anObject[aValue];
    if (result === undefined)
        throw new Error("Invalid parameter value: " + aValue);
    return result;
}
var snapToString = function (snapToString) {
    if (snapToString == undefined || snapToString == null) {
        return "";
    } else {
        return (snapToString + "").trim();
    }
}
var snapToInt = function (strIntValue) {
    var result = parseInt(strIntValue);
    if (isNaN(result)) {
        throw new Error("Invalid parameter value: " + strIntValue);
    }
    return result;
}
