var RobotCommandService = {}


    //
    // setServoColor
    //
    // inputs:
    //   source: bitfield of servo(s) to change
    //     lsb = servo 0, then servo 1, etc.
    //   color: color index:
    //     0 = off
    //     1 = red
    //     2 = green
    //     3 = red-green (lime)
    //     4 = blue
    //     5 = red-blue (purple)
    //     6 = red-green (aqua)
    //     7 = red-green-blue (white)
    //
    // outputs;
    //   corresponding servo LED color on robot is changed
    //   no return value
    //
    


var clone = function(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}
