// This file handles the extra GUI we added to snap's main screen (1 button and 1 modal)
// the when button is pressed we'll look for robots
// uses jQuery and bootstrap 3
// when scan is clicked, show modal

// we grab a modal by ID
var scanModal = $('#modalID')

var alertModal = $('.alert-modal')
// scanModal.modal('show'); // DEBUG
var modalOpen = false
// we grab some button by ID, we will use it later
var btnModal = document.getElementById('openModalViaJS')

var btnSubmit = document.getElementById('btnSubmit')
var modalList = $('#modalSelectionWrap')
var modalMessage = $('#modalMessageWrap')
var modalMessageText = $('#modalMessageContent')
var batteryIcon = $('#batteryIcon')

var robotCanvas = $('#world3g')
var selectedRobot = false // str like 'MECANO####'
var guiState = 'offline' // str: 'connected', 'selecting' , 'handshaking'
var lastBatteryState = 1 // str: 1,2,3,4,5
// var batteryLevelClasses = [ 'fa-battery-full','fa-battery-three-quarters','fa-battery-half','fa-battery-quarter','fa-battery-empty']
var batteryLevelClasses = [ 'fa-battery-empty','fa-battery-quarter','fa-battery-half','fa-battery-three-quarters','fa-battery-full']

var batteryLevelColors = [ 'red','orange','orange','green', 'green']//[ 'green', 'green','orange','orange','red']
// when we click btnModal, open the modal
btnModal.addEventListener('click', function(e){

	onRobotClicked()
}, false)
// main button , can scan or disconnect
onRobotClicked = function (){

	// initial or after disconnection
	// time out is to give ble and bot time for a little reset
	if(guiState == 'offline'){

		setTimeout(function(){
			onScanPressed()
		},50)
	}
	// connect from real bot
	disconnectFromBot()
	// change button mode to offline
	onRobotDisconnected()
	// init modal
	setListContent([])
	modalList.addClass('hidden')
	modalMessage.removeClass('hidden')
	$('#foundTitle').text('')

	// }else if(guiState == 'connected'){ // to disconnect

	// 	// disconnectFromBot()


	// }
}

// reset state to offline if dialog closed without success

scanModal.on('hidden.bs.modal', function (e) {
  if(guiState != 'connected'){

  	onRobotDisconnected()
  }
})

function onMesAlert(msg, title){

	$('#alertTitle').html(title ? title : "MES Meccanoid")
	$('#alertText').html(msg)
	alertModal.modal('show')
}

// callback from drop down, selects a robot
$("#dynamicList").on("click", "li a", function() {

	selectedRobot = $(this).html();
	selectRobotByName(selectedRobot);
	guiState = 'handshaking';
	modalList.addClass('hidden');
	modalMessage.removeClass('hidden');
	let text = RobotConstants.ROBOT_MODEL === RobotConstants.SPIDER_MODEL ? 'Please wait...' : 'Click the yellow button on the Robot...';
	modalMessageText.html('Calling ' + selectedRobot + '<br/>' + text);

});

function onScanPressed(){

	guiState = 'scan'
	refreshStateColor('orange')

	modalMessageText.text('Looking For Robots...')
  	scanModal.modal('show')

    // 1. Check BLE alive
    // 2. Look for robot
    verifyBleServer()
    var initState = "checkingBleServer"
    var startedTimestamp =  Date.now()
		var restartedBleTimeStamp =  Date.now()
		//
    var scanTimerHandle = setInterval(function () {

        var timeElapsed = Date.now() - startedTimestamp
				var restartedElapsed = Date.now() - restartedBleTimeStamp
        //
				// window.java.logFromJs("Scan Timer State: "+ initState)

        switch (initState){

					case  "checkingBleServer":

						if (ApiState.apiAlive == true) {
								// found BLE server - continue
								if (ApiState.blePowered){

										initializeFindRobot()
										initState = "lookingForRobot"
								}else{

										failRobotCheck("Please enable Bluetooth.")
								}


						}
						if (timeElapsed > 2222 && !ApiState.apiAlive) {

								initState = "restartedBle"
								// no BLE server
								restartedBleTimeStamp = Date.now()
								window.java.restartBle()
						}
						break

					case "restartedBle":

						if(restartedElapsed > 2222){

							verifyBleServer()
							initState = "checkingBleRestart"
						}
						break

					case "checkingBleRestart":

						if (ApiState.apiAlive == true) {

								initState = "lookingForRobot"
								// found BLE server - continue
								initializeFindRobot()
						}
						if (restartedElapsed > (2222 + 2222) ) {

								initState = "finished"
								// no BLE server
								failRobotCheck("Bluetooth could not start. <br/>Check Bluetooth USB connection and try again.")

						}
						break

					case	"lookingForRobot":
						if (timeElapsed > 10 * 1000) {

							initState = "finished"
							// could not find robot
							failRobotCheck("Didn\'t find any robot.<br/>Restart the robot and try again.")
						}
						break
					case "finished":

						break
				}
				if(timeElapsed > 15 * 10000){
						clearInterval(scanTimerHandle);
				}

    }, 111);

    modalOpen = true
}

function failRobotCheck (failMessage) {
    if(guiState == 'scan') {
        modalOpen = false;
        scanModal.modal('hide')
        onRobotDisconnected()
        onMesAlert(failMessage)
    }
}

var mes_hideRobotWaitingTimer = 0;
function hideRobotCanvas () {

	robotCanvas.css('visibility', 'hidden');

	if (mes_hideRobotWaitingTimer > 0) {
        clearInterval(mes_hideRobotWaitingTimer);
	}
    mes_hideRobotWaitingTimer = setInterval(function () {
        if (world.children.length == 1) {
            showRobotCanvas();
            clearInterval(mes_hideRobotWaitingTimer);
        }
    }, 222);

}

function showRobotCanvas () {

	robotCanvas.css('visibility', 'initial');

}

function filterRobotNames(robotNames){
	var res = [];
	for (var i = robotNames.length - 1; i >= 0; i--) {
		if( robotNames[i].indexOf(RobotConstants.SUPPORTED_ROBOT_NAME) != -1){
			res.push(robotNames[i]);
		}
	}
	return res;
}


// robots were found, populate list
function onRobotNamesReceived(robotNames){

	robotNames = filterRobotNames(robotNames);
	if(guiState == 'scan' && robotNames.length > 0){

		document.getElementById('ping').play();
		guiState = 'selecting'
		var title = "Found " + robotNames.length + " robot" + (robotNames.length > 1 ? "s" : "") +":"
		$("#foundTitle").text(title)
		setListContent(robotNames)
		modalList.removeClass('hidden')
		modalMessage.addClass('hidden')
	}

}
function refreshStateColor(color){
	$('#bleIcon').css('color', color)
}
function onRobotConnected(name, batteryState){

	if(guiState != 'connected'){ // means we just connected
		console.log('CONNECTED TO BOT')
		setTimeout(function () { RobotControl.saviourPose() },1111 );
	}

	refreshStateColor('green')
	// batteryIcon.removeClass('hidden')

	guiState = 'connected'
	if(modalOpen){
		scanModal.modal('hide')
		modalOpen = false
	}

}

function onRobotHandShakeFailed(){

	if(guiState == 'handshaking'){
		guiState = 'offline'
		refreshStateColor('red')
		selectedRobot = false
		if(modalOpen){
			scanModal.modal('hide')
			modalOpen = false
		}
		onMesAlert("Handshake Failed, try again.")
	}
}

function onRobotDisconnected(){

	// if(guiState = 'connected'){

		guiState = 'offline'
		refreshStateColor('red')

		// changeMessageText('Offline')
		batteryIcon.addClass('hidden')
		$('#buttonText').html('Connect')
		selectedRobot = false
		// disconnectFromBot()
	// }
}

function onBatteryLevelRead(batteryState){

	if (lastBatteryState != batteryState){

		batteryIcon.removeClass(batteryLevelClasses[lastBatteryState])
		batteryIcon.addClass(batteryLevelClasses[batteryState])
		batteryIcon.css('color',batteryLevelColors[batteryState])
		lastBatteryState = clone(batteryState)
		// var color = batteryLevelColors[batteryState]
		// refreshBatteryColor(color)
	}
}

function changeMessageText(msg){

	$('#robotInfo').text(msg)
}
// todo catch modal close event

function setListContent(robotNames){

	var contentStr = "";
	for (var i = robotNames.length - 1; i >= 0; i--) {
		contentStr += '<li><a class="robot_name" href="#">'+robotNames[i]+'</a></li>'
	}
	$('#dynamicList').html(contentStr)
}

$('#myModal').on('hidden.bs.modal', function () {
  // do something…
})
