// ziv
// in robot.js we take care of robot lifcycle and keep state

// current state of different robot parts
var RobotState = RobotConstants.INITIAL_ROBOT_STATE;

var ApiState = {
    apiAlive: false, // reflecs BLE server status
    blePowered: false,
};

var WebGLError = false;

// use error logging to detect problems with 3rd party libs

(function(){
    if(window.console && console.error){
        var old = console.error;
        console.error = function(){
            for(var i = 0;i<=arguments.length -1;i++){
                if(arguments[i].indexOf("Error creating WebGL")!=-1) {
                    WebGLError = true;
                }
            }
            old.apply(this, arguments)
        }
    }  
})();


var lastMotorChange = Date.now()

var numNoAnswer = 0


function verifyBleServer() {
    ApiState.apiAlive = false;
    callRobot(RobotConstants.ALIVE_API, [], onServerAlive, onServerDead)
}
function onServerAlive(res) {
    ApiState.apiAlive = true;
    ApiState.blePowered = res.ble == 'on'
    //onMesAlert("Check  connect BLE dongle and restart the program")
}
function onServerDead() {
    ApiState.apiAlive = false;
    //onMesAlert("Check  connect BLE dongle and restart the program")
}

// start looking for robots
function initializeFindRobot(){

    callRobot(RobotConstants.INIT_API, [], false, false)

}

function pollRobotStatus(){

    setInterval(function(){

        callRobot(RobotConstants.CONNECTION_CHECK_API, [], onConnectionCheck)
        if(RobotState.handShakeStatus == 'handshaked'){
            
            callRobot(
                RobotConstants.WRITE_WITH_RESPONSE_WEAK_API, 
                RobotConstants.CheckStatusCommand, 
                onStatusResult, 
                false)
        }
        if(RobotState.handShakeStatus == 'handshaking'){

            callRobot(
                RobotConstants.WRITE_WITH_RESPONSE_FORCE_API, 
                RobotConstants.HandshakeCommand, 
                onHandshakeResult, 
                false)

        }

    }, RobotConstants.STATUS_POLL_INTERVAL);

}
// start polling
pollRobotStatus();

// holds last handshake timeout callback...
var cancelHandshakeTimeoutHandle = 0;

function selectRobotByName(roboName){

    if (cancelHandshakeTimeoutHandle !== 0) {
        clearTimeout(cancelHandshakeTimeoutHandle);
    }
    cancelHandshakeTimeoutHandle = setTimeout(
        function() {
            cancelHandshake()
        },
        15 * 1000); // 15 seconds
    RobotState.name = roboName;
    callRobot(RobotConstants.SELECT_ROBOT_API, roboName, false, false)
}

// custom maccanoid pairing
function handshakeRobot(){

    callRobot(RobotConstants.WRITE_API, RobotConstants.HandshakeCommand, false, false)
    callRobot(RobotConstants.SUBSCRIBE_API, [], false, false)
}

function disconnectFromBot(){
  RobotState.handShakeStatus = 'none'
  callRobot(RobotConstants.DISCONNECT_API, [], false, false)
}


function onConnectionCheck(response){

    if(response.status == 'found_robots'){

      onRobotNamesReceived(response.robotList)
    }
    if(response.status == 'selected' ){

        if (RobotState.handShakeStatus == 'none') {

            RobotState.handShakeStatus = 'handshaking'
            // changeMessageText('Trying to connect...')
            handshakeRobot()
            // setTimeout(function(){ moved to be called when user selects a robot

            //     cancelHandshake()

            // }, 15 * 1000)
        } else if (RobotState.handShakeStatus == 'handshaked') {


            // (response.name)
            // set GUI
            // changeMessageText('Connected to ' + response.name)
        }

    }
}

// if response from robot is correct, user clicked the yellow button on robot to connect
function onHandshakeResult(response){

    if (response.status &&
        ("data" in response) &&
        (response.data.data[5] == 255)
    ){
        // set state
        RobotState.handShakeStatus = 'handshaked'
        // change GUI
        onRobotConnected()
        // Manually lower robot latency on every connection (fix to mac BLE latency bug)
        callRobot(RobotConstants.CONFIGURE_LATENCY_API, [], false, false)
    }
}

function cancelHandshake(){

  if(RobotState.handShakeStatus != 'handshaked'){ // will be 'handshaking' or 'none'

      RobotState.handShakeStatus = 'none'
      // make sure robot and Gui are synced
      disconnectFromBot() // BOT
      onRobotHandShakeFailed() // GUI
  }

}

function onStatusResult(response){
 
    // RobotState.handshaked = response.status // if we can't get status from robot we are not handshaked
    if(!response.status && (RobotState.handShakeStatus == 'handshaked') ){ // if we lost handshake maybe connection is also down

      console.log("got " + numNoAnswer + " bad responses\r")
      // refreshStateColor('orange')
      if (++numNoAnswer > 3){

        RobotState.handShakeStatus = 'none'
        onRobotDisconnected()
        numNoAnswer = 0
      }

      return
    }
    
    numNoAnswer = 0
    if(typeof(response.data) == 'undefined'){
        // console.log("soft failure")
        return
    }

    RobotState.robotType = response.data.data[1] == 2 ? 'small' : 'big'

    onButtonsResult(response.data.data[8])
    var batteryLevel = response.data.data[13] // TODO CHECK!!
    onBatteryLevelRead(batteryLevel)
    // console.log("onStatusResult", response)
}
