var firstCallTs = Date.now()
var numCalls = 0
var ALLOWED = 50

function throttleAllow(){

  var now = Date.now()
  if(now - firstCallTs > 1000){

    firstCallTs = Date.now()
    numCalls = 0
  }
  ++numCalls
  if(numCalls > ALLOWED){
    // console.log("too many calls!")
    return false
  }

  return true
}

// this is the actual HTTP call.
function callRobot(api, data, onSuccess, onFail){

    if(!throttleAllow()){ return }
    var url = new URL(RobotConstants.ROOT_BLE_URL + api)
    if(data.length > 0){

        var str = Array.isArray(data) ? data.join(",") : data
        url.searchParams.append("d", str)
        // var encodedCommand = api +encodeURIComponent( "?d=" + str)
        // console.log(encodedCommand)
    }else{
        // var encodedCommand = (api)
    }

    var request = new Request(url, {
    	method: 'GET'
    });
    fetch(request)
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
              var resKeys = response.keys()
              for (var i = keys.length - 1; i >= 0; i--) {
                // console.log (keys[i] + " : "+ response[keys[i]])
              }
            // console.log(response)

            return;
          }
          // send response to callback
          response.json().then(function(data) {
              if(onSuccess){
                  onSuccess(data)
              }
          });
        }
      )
      .catch(function(err) {

        if(onFail){
            onFail(err)
        }// console.log('Fetch Error :-S', err);
      });
}
