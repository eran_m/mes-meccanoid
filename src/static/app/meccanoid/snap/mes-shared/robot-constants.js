var RobotConstants = {};
RobotConstants.MES_VERSION = "1.22.78";

console.log("VERSION IS : "+RobotConstants.MES_VERSION)
RobotConstants.ROOT_BLE_URL = "http://localhost:1234"
RobotConstants.INIT_API = "/initialize"
RobotConstants.CLOUD_ACTIVATED = true//false;
// for regular commands. goes to commands queue 
RobotConstants.WRITE_API = "/write"
RobotConstants.SELECT_ROBOT_API = "/select_robot"
// for commands that ask robot for data. will be ignored if commands queue is not empty
RobotConstants.WRITE_WITH_RESPONSE_WEAK_API = "/write_with_response_weak"
// for commands that ask robot for data. goes to commands queue 
RobotConstants.WRITE_WITH_RESPONSE_FORCE_API = "/write_with_response_force"

RobotConstants.CONNECTION_CHECK_API = "/connection_check"
RobotConstants.SUBSCRIBE_API = "/subscribe"
RobotConstants.DISCONNECT_API = "/disconnect_robot"
RobotConstants.CONFIGURE_LATENCY_API = "/configure_latency"
RobotConstants.ALIVE_API = "/alive"
RobotConstants.MOTOR_DELAY = 777
RobotConstants.STATUS_POLL_INTERVAL = 3111
RobotConstants.SERVOS_POLL_INTERVAL = 1111
RobotConstants.IR_POLL_INTERVAL = 222
 
RobotConstants.SPIDER_MODEL = "SPIDER";
RobotConstants.MECCANO_MODEL = "MECCANO";
RobotConstants.MAX_MODEL = "MAX";

var onRobotClicked = function () {
};


RobotConstants.RobotSpeeds = {
    fast: 255,
    medium: 20,
    slow: 10
}

RobotConstants.RobotDirections = {
    FD: 1,
    BK: 2,
    ST: 0
}

var ServoReleaseMode = {
    engaged: 2,
    released: 4
}

var GeneralCommand = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

var robot3dLoaded = false

var TRY_CATCH_XML = '<blocks app="Snap! 4.0, http://snap.berkeley.edu" version="1"><block-definition s="safely try %&apos;action&apos; then if %&apos;error&apos; %&apos;handler&apos;" type="command" category="control"><header></header><code></code><inputs><input type="%cs"></input><input type="%upvar"></input><input type="%cs"></input></inputs><script><custom-block s="let %upvar be %s"><l>reset</l><block s="evaluate"><block s="reportJSFunction"><list><l>proc</l></list><l>var oldHandleError = proc.handleError,&#xD;    oldCatchingErrors = proc.isCatchingErrors;&#xD;&#xD;return function(){&#xD; proc.handleError = oldHandleError;&#xD; proc.isCatchingErrors = oldCatchingErrors;&#xD;}</l></block><list></list></block></custom-block><block s="doCallCC"><block s="reifyScript"><script><block s="doRun"><block s="reportJSFunction"><list><l>reset</l><l>action</l><l>handler</l><l>proc</l></list><l>proc.isCatchingErrors = true;&#xD;proc.handleError = function(error, element){&#xD; reset();&#xD; proc.context = handler;&#xD; proc.context.variables.setVar("error", error);&#xD;}&#xD;&#xD;try{&#xD; proc.evaluate(action, new List(), true);&#xD;}&#xD;catch(e){&#xD; proc.handleError(e, null);&#xD;} </l></block><list><block var="reset"/><block var="action"/><block s="reifyScript"><script><block s="doRun"><block s="reifyScript"><script><block s="doRun"><block var="handler"/><list></list></block><block s="doRun"><block var="return"/><list></list></block></script><list></list></block><list></list></block></script><list></list></block></list></block></script><list><l>return</l></list></block></block><block s="doRun"><block var="reset"/><list></list></block></script></block-definition><block-definition s="error %&apos;msg&apos;" type="command" category="control"><header></header><code></code><inputs><input type="%txt"></input></inputs><script><block s="doRun"><block s="reportJSFunction"><list><l>msg</l></list><l>throw new Error(msg);</l></block><list><block var="msg"/></list></block></script></block-definition><block-definition s="let %&apos;var&apos; be %&apos;val&apos;" type="command" category="other"><header></header><code></code><inputs><input type="%upvar"></input><input type="%s"></input></inputs><script><block s="doSetVar"><l>var</l><block var="val"/></block></script></block-definition></blocks>';
