// <!-- id="rotLeftBtn" -->
$(document).ready(function() {
	$('.rotator-button').click(function(event){
		var direction = (event.target.id == 'rotLeftBtn' || event.target.id ==  'rotLeftIcn') ? 'left' : 'right'

		console.log('direction ' + direction + ' clicked')
		// INVERTED
		if (direction == 'left') {
			
			robot.rotateCameraRight();
			
		} else {
			robot.rotateCameraLeft();
		}
	})
})