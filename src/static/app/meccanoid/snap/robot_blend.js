// RobotStageMorph /////////////////////////////////////////////////////////

RobotStageMorph.prototype = new StageMorph();
RobotStageMorph.prototype.constructor = RobotStageMorph;
RobotStageMorph.prototype.dimensions = new Point(480, 300); // unscaled extent
RobotStageMorph.prototype.frameRate = 0; // unscheduled per default
RobotStageMorph.prototype.setScale = StageMorph.prototype.setScale;

var blendMesh, scene, renderer, helper;

let bonesMapping = {
    'Neck' : 'Fbx01_Head',
    'RArm' : 'Fbx01_R_UpperArm',
    'LArm' : 'Fbx01_L_UpperArm',
    'RElbow' : 'Fbx01_R_Forearm',
    'LElbow' : 'Fbx01_L_Forearm'
}

var robotBones = {};

var clock = new THREE.Clock();

var isFrameStepping = false;
var timeToStep = 0;

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

function findBoneByName(name, bone) {
	if (!bone) {
		return findBoneByName(name, blendMesh.children[0]);
	} else {
		if (bone.name == name) {
			return bone;
		}
		if (bone.children.length > 0) {
			for (var i=0;i<bone.children.length;i++) {
				var b = findBoneByName(name, bone.children[i]);
				if (b) {
					return b;
				}
			}
		}		
	}

}
function RobotStageMorph(globals) {
    this.init(globals);
}

RobotStageMorph.prototype.init = function (globals) {

    //this.setColor(new Color(220, 220, 220));

    var canvas = document.getElementById('world3g');
    var ctx = canvas.getContext('webgl');

    canvas.width = 300;
    canvas.height = 300;
    canvas.style.width  = '300px';
    canvas.style.height = '300px';
    canvas.style.top  = '400px';
    canvas.style.left = '700px';

    this.renderer = new THREE.WebGLRenderer({canvas : canvas, context: ctx});
    //this.renderer.setClearColor( 0x777777 );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.autoClear = true;

    renderer = this.renderer;

    document.body.appendChild( this.renderer.domElement );       

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera( 75, 1, 0.1, 200 );
    this.camera.position.z = 180;
    this.camera.position.y = 80;

	this.scene.add ( new THREE.AmbientLight( 0xffffff ) );

	scene = this.scene;
}

// RobotMorph /////////////////////////////////////////////////////////

RobotMorph.prototype = new Morph();
RobotMorph.prototype.constructor = RobotMorph;
RobotMorph.prototype.paletteColor = new Color(55, 55, 55);
RobotMorph.prototype.thumbnail = SpriteMorph.prototype.thumbnail;
RobotMorph.prototype.blockForSelector = SpriteMorph.prototype.blockForSelector;
RobotMorph.prototype.freshPalette = SpriteMorph.prototype.freshPalette;
RobotMorph.prototype.blockColor = SpriteMorph.prototype.blockColor;

function RobotMorph(globals) {
    this.init(globals);
}

RobotMorph.prototype.init = function (globals) {
    this.name = localize('Robot');
    this.blocksCache = {};
    this.customBlocks = [];
    this.scripts = new ScriptsMorph(this);
};

RobotMorph.prototype.initBlocks = function () {
    RobotMorph.prototype.blocks = {

        // Motion
        moveNeck: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move neck %n degrees',
            defaults: [10]
        },
        moveLShoulder: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move left shoulder %n degrees',
            defaults: [10]
        },
        moveRShoulder: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move right shoulder %n degrees',
            defaults: [10]
        },        
        moveLArm: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move left arm %n degrees',
            defaults: [10]
        },
        moveRArm: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move right arm %n degrees',
            defaults: [10]
        },        
        moveLElbow: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move left elbow %n degrees',
            defaults: [10]
        },
        moveRElbow: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move right elbow %n degrees',
            defaults: [10]
        },        
        moveLFoot: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move left foot %n degrees',
            defaults: [10]
        },
        moveRFoot: {
            only: RobotMorph,
            type: 'command',
            category: 'motion',
            spec: 'move right foot %n degrees',
            defaults: [10]
        },                
    }
}

RobotMorph.prototype.initBlocks();

RobotMorph.prototype.initBlockMigrations = function () {
    RobotMorph.prototype.blockMigrations = {
        doStopAll: {
            selector: 'doStopThis',
            inputs: [['all']]
        },
        doStop: {
            selector: 'doStopThis',
            inputs: [['this script']]
        },
        doStopBlock: {
            selector: 'doStopThis',
            inputs: [['this block']]
        },
        receiveClick: {
            selector: 'receiveInteraction',
            inputs: [['clicked']]
        },
        reportTrue: {
            selector: 'reportBoolean',
            inputs: [true]
        },
        reportFalse: {
            selector: 'reportBoolean',
            inputs: [false]
        }
    };
};
RobotMorph.prototype.initBlockMigrations();

RobotMorph.prototype.blockTemplates = function (category) {
    var blocks = [];

    function block(selector) {
        var newBlock = RobotMorph.prototype.blockForSelector(selector, true);
        newBlock.isTemplate = true;
        return newBlock;
    }

    blocks.push(block('moveNeck'));
    blocks.push(block('moveLShoulder'));
    blocks.push(block('moveRShoulder'));
    blocks.push(block('moveLArm'));
    blocks.push(block('moveRArm'));
    blocks.push(block('moveLElbow'));
    blocks.push(block('moveRElbow'));
    blocks.push(block('moveLFoot'));
    blocks.push(block('moveRFoot'));

    return blocks;
}

RobotMorph.prototype.drawNew = function (globals) {

    this.robotStage = this.parentThatIsA(RobotStageMorph);
    this.image = newCanvas(this.extent());

	container = document.getElementById( 'container' );
	// stats = new Stats();
	// container.appendChild( stats.dom );

	blendMesh = new THREE.BlendCharacter();
    // blendMesh.load( "robot/robot_anims_core_new.json", function() {
    blendMesh.load( "http://localhost:5678/anim.json", function() {

		blendMesh.rotation.y = Math.PI * -135 / 180;
		scene.add( blendMesh );

		var aspect = window.innerWidth / window.innerHeight;
		var radius = blendMesh.geometry.boundingSphere.radius;

		camera = new THREE.PerspectiveCamera( 45, aspect, 1, 10000 );
		camera.position.set( 0.0, radius, radius * 3.5 );

		// controls = new THREE.OrbitControls( camera );
		// controls.target.set( 0, radius, 0 );
		// controls.update();

		// Set default weights
		blendMesh.applyWeight( 'idle', 1 / 3 );
		blendMesh.applyWeight( 'walk', 1 / 3 );
		blendMesh.applyWeight( 'run', 1 / 3 );

		blendMesh.showModel(true);

		helper = new THREE.SkeletonHelper( blendMesh );
		helper.material.linewidth = 3;
		scene.add( helper );

		helper.visible = true;

        // find all bones that should be moved
        for (bone in bonesMapping) {
            robotBones[bone] = findBoneByName(bonesMapping[bone]);
        }

		renderer.render( scene, camera );
	});

	this.mesh = blendMesh;
}

RobotMorph.prototype.redrawSkelton = function() {
    helper.update();
    this.robotStage.renderer.render( this.robotStage.scene, this.robotStage.camera );    
}

RobotMorph.prototype.moveNeck = function(degrees) {
    let bone = robotBones['Neck'];
    if (bone) {
        bone.rotation.z = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveLShoulder = function(degrees) {
    let bone = robotBones['LArm'];
    if (bone) {
        bone.rotation.x = Math.radians(degrees);
        this.redrawSkelton();
    }     
}

RobotMorph.prototype.moveRShoulder = function(degrees) {
    let bone = robotBones['RArm'];
    if (bone) {
        bone.rotation.x = Math.radians(degrees);
        this.redrawSkelton();
    }            
}

RobotMorph.prototype.moveLArm = function(degrees) {
    let bone = robotBones['LArm'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }
}

RobotMorph.prototype.moveRArm = function(degrees) {
    let bone = robotBones['RArm'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }        
}

RobotMorph.prototype.moveLElbow = function(degrees) {
    let bone = robotBones['LElbow'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }         
}

RobotMorph.prototype.moveRElbow = function(degrees) {
    let bone = robotBones['RElbow'];
    if (bone) {
        bone.rotation.y = Math.radians(degrees);
        this.redrawSkelton();
    }     
}

RobotMorph.prototype.moveLFoot = function(degrees) {
    this.mesh.children[0].rotation.z = Math.radians(degrees);
    this.redrawSkelton();        
}

RobotMorph.prototype.moveRFoot = function(degrees) {
    this.mesh.children[0].rotation.z = Math.radians(degrees);
    this.redrawSkelton();    
}

