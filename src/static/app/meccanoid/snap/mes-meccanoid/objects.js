SpriteMorph.prototype.originalInit = SpriteMorph.prototype.init;
SpriteMorph.prototype.init = function(globals) {
    this.originalInit(globals);
};

// Definition of a new Arduino Category

SpriteMorph.prototype.categories.push('meccanoid');
SpriteMorph.prototype.blockColor['meccanoid'] = new Color(236, 28, 45);

SpriteMorph.prototype.originalInitBlocks = SpriteMorph.prototype.initBlocks;
SpriteMorph.prototype.initMeccanoidBlocks = function () {
    // SpriteMorph.prototype.blocks = {
       
        // Meccanoid
        this.blocks.saviourMode = {
            only: SpriteMorph,
            type: 'command',
            category: 'meccanoid',
            spec: '$robot initiate starting position',
        };
        this.blocks.turnServo = {
            only: SpriteMorph,
            type: 'command',
            category: 'meccanoid',
            spec: '$robot turn %servoName motor to %degrees degrees',
            defaults: ['head', 30]
        };
        this.blocks.moveLegs = {
            only: SpriteMorph,
            type: 'command',
            category: 'meccanoid',
            spec: '$robot move %whichLeg %legDirection at %legSpeed speed',
            defaults: ['right leg', 'forward', 'fast']
        };
        this.blocks.stopLegs = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot stop %whichLeg',
            defaults: ['right leg'],
        };
        this.blocks.setEyeColor = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot set eyes color to %ledColor',
            defaults: ['white'],
        };
        this.blocks.setServoLed = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot set %servoName light to %ledColor',
            defaults: ['head', 'white'],
        };
        this.blocks.setBeltLed = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot set %pcbLights MeccaBrain button %pcbState',
            defaults: ['yellow', 'on'],
        };
        this.blocks.releaseServo = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot release %servoName motor',
            defaults: ['head'],
        };
        this.blocks.readServo = {
            only: SpriteMorph,            
            type: 'reporter',
            category: 'meccanoid',
            spec: '$robot read %servoName angle',
            defaults: ['head'],
        };
        this.blocks.reportServoDegreeGreaterThan = {
            type: 'predicate',
            category: 'meccanoid',
            spec: '%servoName angle > %degrees degrees ?',
            defaults: ['head', 30]
        };
        this.blocks.reportServoDegreeLessThan = {
            type: 'predicate',
            category: 'meccanoid',
            spec: '%servoName angle < %degrees degrees ?',
            defaults: ['head', 30]
        };
        this.blocks.runLim = {
            only: SpriteMorph,
            type: 'command',
            category: 'meccanoid',
            spec: '$robot run my L.I.M record %limNumber',
            defaults: [1],
        };
        this.blocks.playPredefined = {
            only: SpriteMorph,            
            type: 'command',
            category: 'meccanoid',
            spec: '$robot play %playActions',
            defaults: ['high five'],
        };
        this.blocks.readIR = {
            only: SpriteMorph,            
            type: 'reporter',
            category: 'meccanoid',
            spec: '$robot measure %whichIR distance',
            defaults: ['left'],
        };
}

SpriteMorph.prototype.initBlocks =  function() {
    this.originalInitBlocks();
    this.initMeccanoidBlocks();
};

SpriteMorph.prototype.initBlocks();

// SpriteMorph block templates
SpriteMorph.prototype.originalBlockTemplates = SpriteMorph.prototype.blockTemplates;

SpriteMorph.prototype.blockTemplates = function (category) {
    var myself = this, blocks = myself.originalBlockTemplates(category);
	    
    function blockBySelector (selector) {
        if (StageMorph.prototype.hiddenPrimitives[selector]) {
            return null;
        }
        var newBlock = SpriteMorph.prototype.blockForSelector(selector, true);
        newBlock.isTemplate = true;
        return newBlock;
    };

    if(category === 'meccanoid'){
        blocks.push(blockBySelector('saviourMode'));
        blocks.push('-');
        blocks.push(blockBySelector('turnServo'));
        blocks.push(blockBySelector('moveLegs'));
        blocks.push(blockBySelector('stopLegs'));
        blocks.push('-');
        blocks.push(blockBySelector('setEyeColor'));
        blocks.push(blockBySelector('setServoLed'));
        blocks.push(blockBySelector('setBeltLed'));
        blocks.push('-');
        blocks.push(blockBySelector('releaseServo'));
        blocks.push('-');
        blocks.push(blockBySelector('readServo'));
        blocks.push(blockBySelector('readIR'));
        blocks.push('-');
        blocks.push(blockBySelector('reportServoDegreeLessThan'));
        blocks.push(blockBySelector('reportServoDegreeGreaterThan'));
        blocks.push('-');
        blocks.push(blockBySelector('runLim'));
        blocks.push(blockBySelector('playPredefined'));
    }
    
    return blocks;
};


// Removing 'other' blocks from 'variables' category
SpriteMorph.prototype.freshPalette = function (category) {
    var palette = new ScrollFrameMorph(null, null, this.sliderColor),
        unit = SyntaxElementMorph.prototype.fontSize,
        x = 0,
        y = 5,
        ry = 0,
        blocks,
        hideNextSpace = false,
        myself = this,
        stage = this.parentThatIsA(StageMorph),
        oldFlag = Morph.prototype.trackChanges;

    Morph.prototype.trackChanges = false;

    palette.owner = this;
    palette.padding = unit / 2;
    palette.color = this.paletteColor;
    palette.growth = new Point(0, MorphicPreferences.scrollBarSize);

    // menu:

    palette.userMenu = function () {
        var menu = new MenuMorph(),
            ide = this.parentThatIsA(IDE_Morph),
            more = {
                operators:
                    ['reifyScript', 'reifyReporter', 'reifyPredicate'],
                control:
                    ['doWarp'],
                variables:
                    [
                        'doDeclareVariables',
                        'reportNewList',
                        'reportCONS',
                        'reportListItem',
                        'reportCDR',
                        'reportListLength',
                        'reportListContainsItem',
                        'doAddToList',
                        'doDeleteFromList',
                        'doInsertInList',
                        'doReplaceInList'
                    ]
            };

        function hasHiddenPrimitives() {
            var defs = SpriteMorph.prototype.blocks,
                hiddens = StageMorph.prototype.hiddenPrimitives;
            return Object.keys(hiddens).some(function (any) {
                return !isNil(defs[any]) && (defs[any].category === category
                    || contains((more[category] || []), any));
            });
        }

        function canHidePrimitives() {
            return palette.contents.children.some(function (any) {
                return contains(
                    Object.keys(SpriteMorph.prototype.blocks),
                    any.selector
                );
            });
        }

        menu.addItem('find blocks...', function () {myself.searchBlocks(); });
        if (canHidePrimitives()) {
            menu.addItem(
                'hide primitives',
                function () {
                    var defs = SpriteMorph.prototype.blocks;
                    Object.keys(defs).forEach(function (sel) {
                        if (defs[sel].category === category) {
                            StageMorph.prototype.hiddenPrimitives[sel] = true;
                        }
                    });
                    (more[category] || []).forEach(function (sel) {
                        StageMorph.prototype.hiddenPrimitives[sel] = true;
                    });
                    ide.flushBlocksCache(category);
                    ide.refreshPalette();
                }
            );
        }
        if (hasHiddenPrimitives()) {
            menu.addItem(
                'show primitives',
                function () {
                    var hiddens = StageMorph.prototype.hiddenPrimitives,
                        defs = SpriteMorph.prototype.blocks;
                    Object.keys(hiddens).forEach(function (sel) {
                        if (defs[sel] && (defs[sel].category === category)) {
                            delete StageMorph.prototype.hiddenPrimitives[sel];
                        }
                    });
                    (more[category] || []).forEach(function (sel) {
                        delete StageMorph.prototype.hiddenPrimitives[sel];
                    });
                    ide.flushBlocksCache(category);
                    ide.refreshPalette();
                }
            );
        }
        return menu;
    };

    // primitives:

    blocks = this.blocksCache[category];
    if (!blocks) {
        blocks = myself.blockTemplates(category);
        if (this.isCachingPrimitives) {
            myself.blocksCache[category] = blocks;
        }
    }

    blocks.forEach(function (block) {
        if (block === null) {
            return;
        }
        if (block === '-') {
            if (hideNextSpace) {return; }
            y += unit * 0.8;
            hideNextSpace = true;
        } else if (block === '=') {
            if (hideNextSpace) {return; }
            y += unit * 1.6;
            hideNextSpace = true;
        } else if (block === '#') {
            x = 0;
            y = ry;
        } else {
            hideNextSpace = false;
            if (x === 0) {
                y += unit * 0.3;
            }
            block.setPosition(new Point(x, y));
            palette.addContents(block);
            if (block instanceof ToggleMorph
                    || (block instanceof RingMorph)) {
                x = block.right() + unit / 2;
                ry = block.bottom();
            } else {
                // if (block.fixLayout) {block.fixLayout(); }
                x = 0;
                y += block.height();
            }
        }
    });

    // global custom blocks:

    if (stage) {
        y += unit * 1.6;

        stage.globalBlocks.forEach(function (definition) {
            var block;
            if (definition.category === category ||
                    (category === 'variables'
                        && contains(
                            ['lists'],
                            definition.category
                        ))) {
                block = definition.templateInstance();
                y += unit * 0.3;
                block.setPosition(new Point(x, y));
                palette.addContents(block);
                x = 0;
                y += block.height();
            }
        });
    }

    // local custom blocks:

    y += unit * 1.6;
    this.customBlocks.forEach(function (definition) {
        var block;
        if (definition.category === category ||
                (category === 'variables'
                    && contains(
                        ['lists'],
                        definition.category
                    ))) {
            block = definition.templateInstance();
            y += unit * 0.3;
            block.setPosition(new Point(x, y));
            palette.addContents(block);
            x = 0;
            y += block.height();
        }
    });

    //layout

    palette.scrollX(palette.padding);
    palette.scrollY(palette.padding);

    Morph.prototype.trackChanges = oldFlag;
    return palette;
};


SpriteMorph.prototype.readIR = function(whichSensor) {

    return robotReadDistanceSensor(whichSensor);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.saviourMode = function() {

    RobotControl.saviourPose();
    // console.log("saviour mode called!")
}

SpriteMorph.prototype.turnServo = function(servoName, degrees) {

    robotSetServoAngle(servoName, degrees);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.moveLegs = function(whichLeg, legsDirection, legsSpeed) {

    robotMotorStart(whichLeg, legsDirection, legsSpeed);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.stopLegs = function(whichLeg) {

    robotMotorStop(whichLeg);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.setEyeColor = function(color) {

    robotSetEyesColor(color);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.setServoLed = function(servoName, color) {

    robotSetServoLight(servoName, color);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.setBeltLed = function(buttonColor, buttonState) {

    robotSetBeltColor(buttonColor, buttonState);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.releaseServo = function(servoName) {

    robotReleaseServo(servoName);
    // console.log("turnServo called!")
}
// SpriteMorph motion - adjustments due to nesting

SpriteMorph.prototype.runLim = function(number) {

    robotPlayMyRecording(number);
    // console.log("turnServo called!")
}

SpriteMorph.prototype.playPredefined = function(actionName) {

    robotPlayPredefinedRecording(actionName);
    // console.log("turnServo called!")
}

// List exporting

WatcherMorph.prototype.userMenu = function () {
    var myself = this,
        menu = new MenuMorph(this),
        subMenu,
        on = '\u25CF',
        off = '\u25CB',
        vNames;

    function monitor(vName) {
        var stage = myself.parentThatIsA(StageMorph),
            varFrame = myself.currentValue.outerContext.variables;
        menu.addItem(
            vName + '...',
            function () {
                var watcher = detect(
                    stage.children,
                    function (morph) {
                        return morph instanceof WatcherMorph
                            && morph.target === varFrame
                            && morph.getter === vName;
                    }
                ),
                    others;
                if (watcher !== null) {
                    watcher.show();
                    watcher.fixLayout(); // re-hide hidden parts
                    return;
                }
                watcher = new WatcherMorph(
                    vName + ' ' + localize('(temporary)'),
                    SpriteMorph.prototype.blockColor.variables,
                    varFrame,
                    vName
                );
                watcher.setPosition(stage.position().add(10));
                others = stage.watchers(watcher.left());
                if (others.length > 0) {
                    watcher.setTop(others[others.length - 1].bottom());
                }
                stage.add(watcher);
                watcher.fixLayout();
            }
        );
    }

    menu.addItem(
        (this.style === 'normal' ? on : off) + ' ' + localize('normal'),
        'styleNormal'
    );
    menu.addItem(
        (this.style === 'large' ? on : off) + ' ' + localize('large'),
        'styleLarge'
    );
    if (this.target instanceof VariableFrame) {
        menu.addItem(
            (this.style === 'slider' ? on : off) + ' ' + localize('slider'),
            'styleSlider'
        );
        menu.addLine();
        menu.addItem(
            'slider min...',
            'userSetSliderMin'
        );
        menu.addItem(
            'slider max...',
            'userSetSliderMax'
        );
        menu.addLine();
        menu.addItem(
            'import...',
            function () {
                var inp = document.createElement('input'),
                    ide = myself.parentThatIsA(IDE_Morph);
                if (ide.filePicker) {
                    document.body.removeChild(ide.filePicker);
                    ide.filePicker = null;
                }
                inp.type = 'file';
                inp.style.color = "transparent";
                inp.style.backgroundColor = "transparent";
                inp.style.border = "none";
                inp.style.outline = "none";
                inp.style.position = "absolute";
                inp.style.top = "0px";
                inp.style.left = "0px";
                inp.style.width = "0px";
                inp.style.height = "0px";
                inp.style.display = "none";
                inp.addEventListener(
                    "change",
                    function () {
                        var file;

                        function txtOnlyMsg(ftype) {
                            ide.inform(
                                'Unable to import',
                                'Snap! can only import "text" files.\n' +
                                    'You selected a file of type "' +
                                    ftype +
                                    '".'
                            );
                        }

                        function readText(aFile) {
                            var frd = new FileReader();
                            frd.onloadend = function (e) {
                                myself.target.setVar(
                                    myself.getter,
                                    e.target.result
                                );
                            };

                            if (aFile.type.indexOf("text") === 0) {
                                frd.readAsText(aFile);
                            } else {
                                txtOnlyMsg(aFile.type);
                            }
                        }

                        document.body.removeChild(inp);
                        ide.filePicker = null;
                        if (inp.files.length > 0) {
                            file = inp.files[inp.files.length - 1];
                            readText(file);
                        }
                    },
                    false
                );
                document.body.appendChild(inp);
                ide.filePicker = inp;
                inp.click();
            }
        );
        if (this.currentValue &&
                (isString(this.currentValue) || !isNaN(+this.currentValue))) {
            menu.addItem('export...', this.valueExporter('plain'));
        } else if (this.currentValue instanceof List) {
            subMenu = new MenuMorph(this.currentValue);
            if (!this.currentValue.contents.some(
                    function (any) {
                        return any instanceof List;
                    })) {
                subMenu.addItem('Plain text', this.valueExporter('plain'));
            }
            subMenu.addItem('JSON', this.valueExporter('json'));
            subMenu.addItem('XML', this.valueExporter('xml'));
            subMenu.addItem('CSV', this.valueExporter('csv'));
            menu.addMenu('export...', subMenu);
        } else if (this.currentValue instanceof Context) {
            vNames = this.currentValue.outerContext.variables.names();
            if (vNames.length) {
                menu.addLine();
                vNames.forEach(function (vName) {
                    monitor(vName);
                });
            }
        }
    }
    return menu;
};

WatcherMorph.prototype.valueExporter = function (format) {
    var myself = this,
        value = this.currentValue,
        contents,
        format = format || 'plain',
        ide = myself.parentThatIsA(IDE_Morph);

    return function () {
        switch (format) {
            case 'plain':
                contents = value instanceof List ? 
                    value.asArray().join('\n') : 
                    value.toString();
                break;
            case 'json':
                contents = JSON.stringify(value);
                break;
            case 'xml':
                contents = ide.serializer.serialize(value);
                break;
            case 'csv':
                try {
                    contents = value.toCSV();
                } catch (err) {
                    if (Process.prototype.isCatchingErrors) {
                        ide.showMessage('List cannot be converted into CSV', 2);
                    } else {
                        throw err;
                    }
                    return;
                }
                break;
        }

        ide.saveFileAs(
            contents,
            'text/' + format + ';charset=utf-8',
            myself.getter // variable name
            );
    };
};
