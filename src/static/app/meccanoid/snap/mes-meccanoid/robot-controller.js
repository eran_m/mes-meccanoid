// ziv
// here we interact with a robot / animation / both
//

var lastMotorsBuffer = clone(RobotConstants.SetMotorsCommand);
var isWalking = false
// var isUpdateServos = false

// RobotControl methods are the API to the physical robot. 
// 
var RobotControl = {};

// local refs of widely-used constants
var RobotDirections = RobotConstants.RobotDirections;
var ServoColors = RobotConstants.ServoColors;
var ServoLocations = RobotConstants.ServoLocations;
var MotorLocations = RobotConstants.MotorLocations;
var PCBLedNames = RobotConstants.PCBLedNames;


RobotControl.saviourPose = function() {

    // if servo is released - call Engage first and then call setServo !
    var aServerIsReleased = false;
    for (var i = 1; i <= 8; i++) {
        if (RobotState.servoStatus[i] === ServoReleaseMode.released) {
            aServerIsReleased = true;
        }
    }
    if (aServerIsReleased && (RobotState.handShakeStatus == 'handshaked')) {

        console.log("At last one servo is not engaged! trying to engage all now");
        this.setReleaseServoAll('engaged');
        callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, function () {
            setTimeout(
                function () {
                    internalSaviourPose(this);
                }, 244);
        }, false);

    } else {

        internalSaviourPose(this);

    }
}

RobotControl.setServoPositions = function(servoName, deg) {

    // if servo is released - call Engage first and then call setServo !
    if (RobotState.servoStatus[ServoLocations[RobotState.robotType][servoName]] == ServoReleaseMode.released) {

        console.log(servoName + " servo is not engaged! trying to engage now")
        RobotState.servoStatus[ServoLocations[RobotState.robotType][servoName]] = ServoReleaseMode.engaged; // engage
        callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, function () {
            setTimeout(
                function(){

                     internalSetServoPositions(servoName, deg);   
                }, 444);
        }, false)

    } else {

        internalSetServoPositions(servoName, deg);

    }    
}

function onIRResult(response){
    
    if (!response.status || !response.data) {
        return
    }
    RobotState.IRState.left = response.data.data[2];
    RobotState.IRState.right = response.data.data[1];

}


RobotControl.getIRStatus = function(whichSensor) {
    
    return RobotState.IRState[whichSensor];
}

RobotControl.walk = function(feet, direction, speed) {
    var data = RobotConstants.SetMotorsCommand;
    // snap dropdown adds slashes :(
    direction = direction.trim()
    speed = speed.trim()
    var dirCommand = RobotDirections[direction]
    var speedCommand = getSpeed(direction, speed)

    // movementAnimation(feet, direction, speed)

    if (feet == 'both' || feet == 'right') {
        data[MotorLocations.right.direction] = dirCommand
        data[MotorLocations.right.speed] = speedCommand

    }
    if (feet == 'both' || feet == 'left') {
        data[MotorLocations.left.direction] = dirCommand
        data[MotorLocations.left.speed] = speedCommand
    }
    RobotState.motors = data
    isWalking = true

}

// change colors of lights
// invocation: setRobotLight('rightArm', 'green')
RobotControl.setRobotLight = function(servoName, colorName) {
    // console.log("should change "+ servoName + " to "+ colorName)

    var data = getServoLightCommandArr(ServoLocations[RobotState.robotType][servoName], ServoColors[colorName])
    callRobot(RobotConstants.WRITE_API, data, false, false)
}


RobotControl.getPCBLed = function(ledName) {
    return RobotState.PCBStatus[PCBLedNames[ledName]] === 1;
}


RobotControl.setPCBLed = function(ledName, state) {

    var lightOn = state ? 1 : 0
    RobotState.PCBStatus[PCBLedNames[ledName]] = lightOn

    callRobot(RobotConstants.WRITE_API, RobotState.PCBStatus, false, false)
}

// Turn all on/off
RobotControl.setPCBLeds = function(state) {

    var lightOn = state ? 1 : 0
    var pcbLedsNames = Object.keys(PCBLedNames);
    for (var i = 0; i < pcbLedsNames.length; i++) {
        var ledName = pcbLedsNames[i];
        RobotState.PCBStatus[PCBLedNames[ledName]] = lightOn;
    }
    callRobot(RobotConstants.WRITE_API, RobotState.PCBStatus, false, false)
}

RobotControl.setRobotEyesColor = function(colorName) {

    var rgb = RobotConstants.RgbColors[colorName]
    var bytes = rgbToTwoBytes(rgb)
    // var cmd = RobotConstants.EyesColorCommand
    RobotState.eyes[1] = RobotState.eyes[3] = bytes.greenRedByte
    RobotState.eyes[2] = RobotState.eyes[4] = bytes.timeBlueByte
    robot.setEyesColor(rgb.r, rgb.g, rgb.b)
    callRobot(RobotConstants.WRITE_API, RobotState.eyes, false, false)
}

RobotControl.forceServoDegrees = function() {

    var gotAnswers = false
    callRobot(RobotConstants.WRITE_WITH_RESPONSE_FORCE_API, RobotConstants.CheckServosCommand, function(response){
        onServosResult(response)
       
    }, function(error){
       return "N/A BAD RESPONSE";
    })

    return;
}


RobotControl.getServoPositionDegrees = function(servoName) {

    // where to look for degree
    var locationInBuffer = ServoLocations[RobotState.robotType][servoName]
    // the dergree that was saved on last change
    var degFromRobot = RobotState.servoPositions[locationInBuffer]
    // convert from robot-value to gui-degrees angle
    var degForUserGui = transformServoAngleToDegrees(servoName, degFromRobot)
    ////console.log(servoName + ' robot: ' + degFromRobot + " -> angle: " + degForUserGui);
    return degForUserGui;
}

RobotControl.getServoPositionRawValue = function(servoName) {

    // where to look for degree
    var locationInBuffer = ServoLocations[RobotState.robotType][servoName];
    // the dergree that was saved on last change
    var degFromRobot = RobotState.servoPositions[locationInBuffer];
    return degFromRobot;
}


RobotControl.setReleaseServoAll = function(mode) {

    for (var i = RobotState.servoStatus.length - 1; i >= 1; i--) {
        var servoMode = (mode == 'release' ? ServoReleaseMode.released : ServoReleaseMode.engaged);
        RobotState.servoStatus[i] = servoMode
    }
    callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, false, false)
}


RobotControl.playPreset = function(presetCode, presetParam) {

    var cmd = clone(GeneralCommand)
    cmd[0] = RobotConstants.MainCommands.MB_PlayPreset
    cmd[1] = presetCode
    cmd[3] = presetParam ? presetParam : 0

    callRobot(RobotConstants.WRITE_API, cmd, false, false)
}

RobotControl.playLim = function(limNumber) {
    var cmd = clone(GeneralCommand)
    cmd[0] = RobotConstants.MainCommands.MB_PlayLIM
    cmd[1] = limNumber
    callRobot(RobotConstants.WRITE_API, cmd, false, false)
}

RobotControl.setReleaseServo = function(servoName, mode) {

    var servoMode = (mode == 'release' ? ServoReleaseMode.released : ServoReleaseMode.engaged);
    RobotState.servoStatus[ServoLocations[RobotState.robotType][servoName]] = servoMode;
    callRobot(RobotConstants.WRITE_API, RobotState.servoStatus, false, false)
}

// update and return current servo light status
var getServoLightCommandArr = function (servo, color){

    for(var i = 1; i < 16; i++){
        // if it's the light we want to change, update proper place in state array. otherwise keep state same
        RobotState.servoLights[i] = (i == servo ? color : RobotState.servoLights[i])
        
    }
    return RobotState.servoLights
}


function internalSetServoPositions(servoName, degree) {
    // 3D
    callAnimation(servoName, degree);
    //
    // Real robot
    // servo get 'angle' values of 0..225
    var robotDegree = transformDegreeToServoAngle(servoName, degree);
    //console.log(servoName + ' angle: ' + degree + " -> robot: " + robotDegree);

    var slot = ServoLocations[RobotState.robotType][servoName];
    RobotState.servoPositions[slot] = robotDegree

    var data = RobotState.servoPositions;
    callRobot(RobotConstants.WRITE_API, data, false, false)
}

function callAnimation(servoName, d) {

    // var d =( degree + SERVO_DEGREE_OFFSET ) / SERVO_DEGREE_RATIO

    switch (servoName) {

        case "rightArm":
            robot.moveRArm(d)
            break
        case "leftArm":
            robot.moveLArm(d)
            break
        case "rightElbow":
            robot.moveRElbow(d)
            break
        case "leftElbow":
            robot.moveLElbow(d)
            break
        case "rightShoulder":
            robot.moveRShoulder(d)
            break
        case "leftShoulder":
            robot.moveLShoulder(d)
            break
        case "neck":
            robot.moveNeck(d)
            break
        case "head":
            robot.moveHead(d)
            break
        case "leftArm":
            robot.moveHead(d)
            break
    }
}
// send correct move command to 3d bot
function updateMovementAnimation() {

    if (!robot3dLoaded) {
        return
    }
    var rightDirection = RobotState.motors[MotorLocations.right.direction]
    var leftDirection = RobotState.motors[MotorLocations.left.direction]
    var rightSpeed = RobotState.motors[MotorLocations.right.speed]
    var leftSpeed = RobotState.motors[MotorLocations.left.speed]
    var higherSpeed = rightSpeed > leftSpeed ? rightSpeed : leftSpeed

    //console.log("rightDirection " + rightDirection +", leftDirection: "+leftDirection)
    // moving straight ahead/backwards
    if (rightDirection == leftDirection && rightSpeed == leftSpeed) {

        if (rightDirection != RobotDirections.ST) {

            robot.moveLegs(rightSpeed, rightDirection)
        } else {

            //console.log("should stop rotate")
            robot.stopMovement()
        }
    // moving to side
    } else if (rightDirection != leftDirection) {
        //console.log("should rotate - legs direction not same")
        if (rightDirection == RobotDirections.FD) {
            //console.log("should rotateLeft, right moves forward")

            robot.rotateLeft(higherSpeed)
        } else if (rightDirection == RobotDirections.BK) {
            //console.log("should rotateRight, right moves back")

            robot.rotateRight(higherSpeed)
        } else if (leftDirection == RobotDirections.FD) {
            //console.log("should rotateRight, left moves forward")

            robot.rotateRight(higherSpeed)
        } else if (leftDirection == RobotDirections.BK) {
            //console.log("should rotateLeft, left moves back")

            robot.rotateLeft(higherSpeed)
        }

    } else if (rightDirection == leftDirection && rightSpeed != leftSpeed) {
        //console.log("should rotate - legs speed not same")

        if (rightSpeed > leftSpeed) {

            robot.rotateLeft(higherSpeed)
        } else {

            robot.rotateRight(higherSpeed)
        }
    }
}


function rgbToTwoBytes(rgb) {
    var time = 0 // not in use so always 0 .. if used shpuld be 0 - 7
    var greenRedByte = ((rgb.g >> 5) << 3) | (rgb.r >> 5)
    var timeBlueByte = time << 3 | (rgb.b >> 5)
    return {'greenRedByte': greenRedByte, 'timeBlueByte': timeBlueByte}
}


function getSpeed(direction, speed) {

    if (direction == 'ST') {
        return 0
    }
    return RobotConstants.RobotSpeeds[speed]
}

// Convert angle from Robot -> Real
function transformServoAngleToDegrees(servoName, rawAngleFromRobot) {
    // change from 0->225 range to -90->90 range
    var servoTable = RobotConstants.ServoAngleToRealDegrees[servoName];
    // iterate the structude and find the correct entry we 'fit-in'
    // find smaller & bigger entries (one before and one after current read value)
    var biggerValue = undefined; // bigger entry
    var smallerValue = undefined; // smaller entry
    for (var i = 0; i < servoTable.robotAngle.length; i++) {
        var currentRawValue = servoTable.robotAngle[i];
        if ((currentRawValue >= rawAngleFromRobot) && (biggerValue == undefined || currentRawValue <= biggerValue.robotAngle))
            biggerValue = {robotAngle: currentRawValue, realAngle: servoTable.realAngle[i]};
        if ((currentRawValue <= rawAngleFromRobot) && (smallerValue == undefined || currentRawValue >= smallerValue.robotAngle))
            smallerValue = {robotAngle: currentRawValue, realAngle: servoTable.realAngle[i]};
    }
    if (biggerValue === undefined)
        return smallerValue.realAngle; // only smaller entry
    if (smallerValue === undefined)
        return biggerValue.realAngle; // only bigger entry
    if (biggerValue.realAngle == smallerValue.realAngle)
        return biggerValue.realAngle; // cannot divide by zero
    // Now using basic math we have (x1,y1) and (x2,y2)...
    // y=ax+b , x=(y1-y2)/(x1-x2) , b=y-ax
    var a = (biggerValue.robotAngle - smallerValue.robotAngle) / (biggerValue.realAngle - smallerValue.realAngle);
    var b = biggerValue.robotAngle - a * biggerValue.realAngle;
    // x=(y-b)/a
    var result = Math.trunc((rawAngleFromRobot - b) / a);
    return result;
}

// Convert angle from Real -> Robot
function transformDegreeToServoAngle(servoName, realAngleDegrees) {
    // change from 0->225 range to -90->90 range
    var servoTable =RobotConstants.ServoAngleToRealDegrees[servoName];
    // iterate the structude and find the correct entry we 'fit-in'
    // find smaller & bigger entries (one before and one after current read value)
    var biggerValue = undefined; // bigger entry
    var smallerValue = undefined; // smaller entry
    for (var i = 0; i < servoTable.realAngle.length; i++) {
        var currentRealValue = servoTable.realAngle[i];
        if ((currentRealValue >= realAngleDegrees) && (biggerValue == undefined || currentRealValue <= biggerValue.realAngle))
            biggerValue = {robotAngle: servoTable.robotAngle[i], realAngle: currentRealValue};
        if ((currentRealValue <= realAngleDegrees) && (smallerValue == undefined || currentRealValue >= smallerValue.realAngle))
            smallerValue = {robotAngle: servoTable.robotAngle[i], realAngle: currentRealValue};
    }
    if (biggerValue === undefined)
        return smallerValue.robotAngle; // only smaller entry
    if (smallerValue === undefined)
        return biggerValue.robotAngle; // only bigger entry
    if (biggerValue.robotAngle == smallerValue.robotAngle)
        return biggerValue.robotAngle; // cannot divide by zero
    // Now using basic math we have (x1,y1) and (x2,y2)...
    // y=ax+b , x=(y1-y2)/(x1-x2) , b=y-ax
    var a = (biggerValue.realAngle - smallerValue.realAngle) / (biggerValue.robotAngle- smallerValue.robotAngle);
    var b = biggerValue.realAngle - a * biggerValue.robotAngle;
    // x=(y-b)/a
    var result = Math.trunc((realAngleDegrees - b) / a);
    return result;
}



function internalSaviourPose (controller) {
    
    var servos = Object.keys(ServoLocations[RobotState.robotType]);
    controller.setRobotEyesColor('off');
    for (var i = 0; i < servos.length; i++) {
        internalSetServoPositions(servos[i], 0);
        controller.setRobotLight(servos[i], ServoColors.off);
    }
    controller.setPCBLeds(false);
    controller.walk('both', 'ST', 'fast');
    robot.resetPosition()

}

/*
// Saviour mode flow:
//   1. Call release all motors
//   2. Wait & Check periodically until all motors are released
//   3. Call saviour mode initializations (lights, motors etc.)
var internal_saviourPoseWaitingToContinue_flag = false;

function saviourPose() {

    setReleaseServoAll('engaged')
    internal_saviourPoseWaitingToContinue_flag = (RobotState.handShakeStatus == 'handshaked');
    let saviourStartTs = Date.now(); 
    let servosInterval = setInterval(function () {
        
        if(( Date.now() - saviourStartTs ) > 1000){
            internal_saviourPoseWaitingToContinue_flag = false;
            console.log("Warning: saviour mode didnt engage all motors for 1 sec...");
        }

        if (!internal_saviourPoseWaitingToContinue_flag){

            var servos = Object.keys(ServoLocations[RobotState.robotType])
            setRobotEyesColor('off')
            for (var i = 0; i < servos.length; i++) {
                internalSetServoPositions(servos[i], 0)
                setRobotLight(servos[i], ServoColors.off)
            }
            setPCBLeds(false);
            walk('both', 'ST', 'fast');
            robot.resetPosition()
            clearInterval(servosInterval);
        }
    },10);  

}

function waitForAllEngaged(response){

    var allEngaged = true;
    if (!response.status || !response.data) {
        return
    }
    for (var i = 1; i < 9; i++) {
        RobotState.servoStatus[i] = response.data.data[i]
        if (RobotState.servoStatus[i] === ServoReleaseMode.released) {
            //console.log("not all engaged!");
            allEngaged = false;
        }

    }
    //console.log("all engaged!");
    internal_saviourPoseWaitingToContinue_flag = internal_saviourPoseWaitingToContinue_flag && !allEngaged;
}

// check servos status for "all engaged"
setInterval(function () {

    // read info from robot
    if (RobotState.handShakeStatus == 'handshaked' && internal_saviourPoseWaitingToContinue_flag) {

        callRobot(RobotConstants.WRITE_WITH_RESPONSE_FORCE_API, ServoStatusCheckCommand, waitForAllEngaged, false);
    }
}, SERVOS_POLL_INTERVAL/ 3);
*/

function testPCB() {
    var index = 0
    var keys = Object.keys(PCBLedNames)
    setInterval(function () {
        if (index < keys.length) {

            var isOn = RobotState.PCBStatus[PCBLedNames[keys[index]]] == 1
            turnTo = isOn == 0 ? true : false
            setPCBLed(keys[index], turnTo)
            index++
        } else {
            index = 0
        }
    }, 200)
}
 

function onServosResult(response) {

    // unLockApi('MB_GetServoPos')
    if (!response.status || !response.data) {
        return
    }
    var changed = false
    for (var i = 1; i < 10; i++) {
        if (RobotState.servoPositions[i] != response.data.data[i]) {

            changed = true

        }
        RobotState.servoPositions[i] = response.data.data[i]
        var name = servoNameForLocation(i, RobotState.robotType);
        if (typeof name !== 'undefined'){
            var d = RobotControl.getServoPositionDegrees(name);
            callAnimation(name, d); 
        }
    }

    var now = Date.now()
    var sinceMS = now - RobotState.lastServoStatus
    RobotState.lastServoStatus = now;
    var debugString = RobotState.servoPositions.join(',') ;
    // $('#debug_content').text(debugString)
    // $('#debug_content2').text(sinceMS / 1000)
    // 
}

function onButtonsResult(byte) {

    var newButtonState = {
        blueButton: (byte & 0x01) != 0,
        redButton: (byte & 0x02) != 0,
        greenButton: (byte & 0x04) != 0,
        yellowButton: (byte & 0x08) != 0
    }

    var changed = false
    Object.keys(newButtonState).forEach(function (k, i) {
        if (newButtonState[k] != RobotState.buttonState[k]) {

            changed = true
            console.log(k + ( newButtonState[k] ? ' was pressed' : 'was released' ))
        }
    })

    if (changed) {

        RobotState.buttonState = newButtonState
    }

}

function getButtonState() {

    return RobotState.buttonState
}

function motorChanged() {

    var changed = false
    for (var i = lastMotorsBuffer.length - 1; i >= 0; i--) {
        if (lastMotorsBuffer[i] != RobotState.motors[i]) {
            changed = true
        }
    }
    return changed
}
function motorRunning(){
  for (var i = 1; i < RobotState.motors.length; i++) {

    if(RobotState.motors[i] != 0){
      return true
    }
  }
  return false
}

// continously send motor commands to bot
//
// since snap wait is at least 1 second, any motor command blocks running less then 1s apart will
// override\add to each other
// this allows user to set each leg motor with a different block for each leg, and both changes
// will take place simultanously
// loop is 10ms to start quickly, but will actually write only once in 1s

setInterval(function () {

    var elapsedMS = Date.now() - lastMotorChange
    if (elapsedMS < RobotConstants.MOTOR_DELAY) {
        return
    }

    updateMovementAnimation()

    if (RobotState.handShakeStatus == 'handshaked' && isWalking) {

        lastMotorsBuffer = clone(RobotState.motors)
        // callRobot(ALIVE_API, [],  false, false)
        callRobot(RobotConstants.WRITE_API, RobotState.motors, false, false)
        lastMotorChange = Date.now()
        isWalking = motorRunning()
    }

}, 10) // TODO CHANGE TO CONST

// check servos positions
setInterval(function () {

    // read info from robot   
    if (RobotState.handShakeStatus == 'handshaked') {

        callRobot(RobotConstants.WRITE_WITH_RESPONSE_WEAK_API, RobotConstants.CheckServosCommand, onServosResult, false)
    }
}, RobotConstants.SERVOS_POLL_INTERVAL); 


// check servos status
setInterval(function () {

    // read info from robot   
    if (RobotState.handShakeStatus == 'handshaked') {

        callRobot(RobotConstants.WRITE_WITH_RESPONSE_WEAK_API, RobotConstants.IRReadCommand, onIRResult, false)
    }
}, RobotConstants.IR_POLL_INTERVAL); 