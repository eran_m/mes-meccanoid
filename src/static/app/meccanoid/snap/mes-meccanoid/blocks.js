
SyntaxElementMorph.prototype.originalLabelPart = SyntaxElementMorph.prototype.labelPart;

SyntaxElementMorph.prototype.labelPart = function (spec) {
    var part, tokens, myself = this;
    
    var meccSpecs = ['%playActions', '%limNumber', '%pcbState', '%pcbLights','%ledColor', '%whichLeg', '%whichIR', '%legDirection', '%legSpeed', '%servoName', '%degrees']

    if (meccSpecs.indexOf(spec) === -1){
        return myself.originalLabelPart(spec);
    }
    if (spec[0] === '%' &&
            spec.length > 1 &&
            (this.selector !== 'reportGetVar' ||
                (spec === '%turtleOutline' && this.isObjInputFragment()))) {

        // check for variable multi-arg-slot:
        if ((spec.length > 5) && (spec.slice(0, 5) === '%mult')) {
            part = new MultiArgMorph(spec.slice(5));
            part.addInput();
            return part;
        }

        // single-arg and specialized multi-arg slots:
        switch (spec) {
       
        // Meccanoid params

        case '%playActions':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                    'high five' : ['high five'],
                    'shake hands' : ['shake hands'],
                    'walk with me' : ['walk with me'],
                    'hug me' : ['hug me'],
                    'dance with me' : ['dance with me'],
                    'exercise' : ['exercise'],
                    'do a dance' : ['do a dance'],
                    'do kung fu' : ['do kung fu'],
                    'tell a joke' : ['tell a joke'],
                    'stop current activity' : ['stop current activity'],
                    'system check' : ['system check'],
                },
                true // read-only
            );
            break;
        case '%limNumber':
            part = new InputSlotMorph(
                null, // text
                true, // numeric?
                {
                    '1': [1],
                    '2': [2],
                    '3': [3],
                    '4': [4],
                    '5': [5],
                    '6': [6],
                    '7': [7],
                    '8': [8],
                    '9': [9],
                    '10': [10],

                },
                false // read-only
            );
            break;
        case '%pcbState':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                    'on': ['on'],
                    'off': ['off'], 
                },
                true // read-only
            );
            break;
        case '%pcbLights':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                    'yellow': ['yellow'],
                    'green': ['green'], 
                    'blue': ['blue'],
                    'red': ['red'],
                },
                true // read-only
            );
            break;
        case '%ledColor':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                    'white': ['white'],
                    'yellow': ['yellow'],
                    'green': ['green'], 
                    'cyan': ['cyan'],
                    'blue': ['blue'],
                    'purple': ['purple'],
                    'red': ['red'],
                    'off': ['off']
                },
                true // read-only
            );
            break;
        case '%whichLeg':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                  'right leg':['right leg'],
                  'left leg':['left leg'],
                  'both legs':['both legs'],
                },
                true // read-only
            );
            break;
        case '%whichIR':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                  'right':['right'],
                  'left':['left'],
                },
                true // read-only
            );
            break;
        case '%legDirection':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                  'forward':['forward'],
                  'backward':['backward'],
                },
                true // read-only
            );
            break;
        case '%legSpeed':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                  'fast':['fast'],
                  // 'medium':['medium'],
                  'slow':['slow'],

                },
                true // read-only
            );
            break;
        case '%servoName':
            part = new InputSlotMorph(
                null, // text
                false, // numeric?
                {
                    'head': ['head'],
                    'neck': ['neck'],
                    'right shoulder': ['right shoulder'],
                    'right arm': ['right arm'],
                    'right elbow': ['right elbow'],
                    'left shoulder': ['left shoulder'],
                    'left arm': ['left arm'],
                    'left elbow': ['left elbow'],                                                      
                },
                true // read-only
            );
            break;
        case '%degrees': // not read-only!
        part = new InputSlotMorph(
            null,
            true,
            {
                ' 90' : 90,
                ' 60' : 60,
                ' 30' : 30,
                ' 0'   : '0',
                ' -30' : -30,
                ' -60' : -60,
                ' -90' : -90,
            }
        );
        part.setContents(90);
        break;
            // END Meccanoid params

        default:
            nop();
        }
    } else if (spec[0] === '$' &&
            spec.length > 1 &&
            this.selector !== 'reportGetVar') {
/*
        // allow costumes as label symbols
        // has issues when loading costumes (asynchronously)
        // commented out for now

        var rcvr = this.definition.receiver || this.receiver(),
            id = spec.slice(1),
            cst;
        if (!rcvr) {return this.labelPart('%stop'); }
        cst = detect(
            rcvr.costumes.asArray(),
            function (each) {return each.name === id; }
        );
        part = new SymbolMorph(cst);
        part.size = this.fontSize * 1.5;
        part.color = new Color(255, 255, 255);
        part.isProtectedLabel = true; // doesn't participate in zebraing
        part.drawNew();
*/

        // allow GUI symbols as label icons
        // usage: $symbolName[-size-r-g-b], size and color values are optional
        // If there isn't a symbol under that name, it just styles whatever is
        // after "$", so you can add unicode icons to your blocks, for example
        // ☺️
        tokens = spec.slice(1).split('-');
        if (!contains(SymbolMorph.prototype.names, tokens[0])) {
            part = new StringMorph(tokens[0]);
            part.fontName = this.labelFontName;
            part.fontStyle = this.labelFontStyle;
            part.fontSize = this.fontSize * (+tokens[1] || 1);
        } else {
            part = new SymbolMorph(tokens[0]);
            part.size = this.fontSize * (+tokens[1] || 1.2);
        }
        part.color = new Color(
            +tokens[2] === 0 ? 0 : +tokens[2] || 255,
            +tokens[3] === 0 ? 0 : +tokens[3] || 255,
            +tokens[4] === 0 ? 0 : +tokens[4] || 255
        );
        part.isProtectedLabel = tokens.length > 2; // zebra colors
        part.shadowColor = this.color.darker(this.labelContrast);
        part.shadowOffset = MorphicPreferences.isFlat ?
                new Point() : this.embossing;
        part.drawNew();
    } else {
        part = new StringMorph(
            spec, // text
            this.fontSize, // fontSize
            this.labelFontStyle, // fontStyle
            true, // bold
            false, // italic
            false, // isNumeric
            MorphicPreferences.isFlat ?
                    new Point() : this.embossing, // shadowOffset
            this.color.darker(this.labelContrast), // shadowColor
            new Color(255, 255, 255), // color
            this.labelFontName // fontName
        );
    }
    return part;
};
