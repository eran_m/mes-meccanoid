
var express = require('express')
var app = express()
let isWin = /^win/.test(process.platform)

var console = {};
console.log = function(){};
console.warn = function(){};
console.error = function(){};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // console.log('in headers ', res)
  next();
});

// to serve our xml staticly to client
var resourceDirPath = '.'+(isWin ? '\\' : '/')+'meccanoid'
app.use(express.static(resourceDirPath))

app.get('/alive', function (req, res, next) {

    res.send({status:true,msg:'server active'})
})

app.get('/kill', function (req, res, next) {
  process.exit()
})

app.listen(5678, function () {
//   log.info('BLE server loaded successfully!')
})
