var path = require('path');

  module.exports = {
    entry: './app.js',
    target:'node',
    output: {
      filename: 'static-server-mac-bundle.js',
      path: path.resolve(__dirname, '')
    },
    node: {
    	fs: "empty",
    	net: "mock"
  	},
    module:  {
      exprContextCritical: false,
      loaders:[
      { test: /\.json$/,
          loader: 'json-loader'
        },]
    },
  };
