import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.Constants;
import org.json.simple.JSONObject;

import java.io.FileReader;
import java.net.URISyntaxException;

import javax.swing.*;

/**
 * Created by zivtaller on 13/08/2017.
 */
public class SplashScreenLoader extends Preloader {

    private Stage splashScreen;
    private ImageView _splashImageView;
    private String _imageFileName;
    @Override
    public void start(Stage stage) throws Exception {

        this.loadConfig();
        splashScreen = stage;

        splashScreen.initStyle(StageStyle.UTILITY);
        Image image = null;
        try {
            String u =getClass().getResource(_imageFileName).toURI().toString();
            image = new Image(u);
        } catch (URISyntaxException e) {
            e.printStackTrace();
//            LOGGER.severe(e.getMessage());
        }
        _splashImageView = new ImageView();
        _splashImageView.setImage(image);

        splashScreen.setTitle("Preparing MES");
//        splashScreen.getIcons().add(new Image("file:icon.png"));
        splashScreen.setScene(createScene());
        splashScreen.show();
    }

    public Scene createScene() {
        StackPane root = new StackPane();
        root.getChildren().addAll(_splashImageView);
        Scene scene = new Scene(root, 1024, 768);
        return scene;
    }

    private void loadConfig(){
        org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
        try {
            Object obj = parser.parse(new FileReader( System.getProperty("user.dir")+ Constants.LOCAL_SLASH_CHAR+Constants.JAR_NAME+".config"));
            JSONObject jsonObject = (JSONObject)obj;
            _imageFileName = (String)jsonObject.getOrDefault("imageFileName", "");
        } catch (Exception e) {
            e.printStackTrace();
//            LOGGER.severe(e.getMessage());
        }
    }
//    @Override
//    public void handleApplicationNotification(PreloaderNotification notification) {
//        System.out.println("here");
//        if (notification instanceof StateChangeNotification) {
//            splashScreen.hide();
//        }
//    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification scn) {
//        System.out.println("here2");

        if (scn.getType() == StateChangeNotification.Type.BEFORE_START) {
            splashScreen.hide();
        }
    }


}
