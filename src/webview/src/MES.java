
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.events.ConsoleEvent;
import com.teamdev.jxbrowser.chromium.events.ConsoleListener;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;
import com.teamdev.jxbrowser.chromium.internal.Environment;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import javafx.application.Application;


import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Optional;
import java.util.Timer;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static java.lang.System.*;
import java.io.FileReader;
import java.io.IOException;

import main.*;
import org.json.simple.JSONObject;

public class MES extends Application implements AppFromJS {

    public JsBridge jsb;
    private MesServicesBridge mesBridge;
    public Browser browser;
    public static final Logger LOGGER = Logger.getLogger( MES.class.getName() );
    private FileHandler fh;
    private MesTimerTask _loadingTimeout;
    private boolean _isJsLoaded = false;
    private ImageView _splashImageView;
    private String _browserType;
    private String _logLevel;
    protected boolean isWebGLBroken = false;
    private String _guiEntryPointUrl;
    private String _iconUrl;
    private String _imageFileName;

    public void init() throws Exception {

        // Important to set this preference BEFORE instantiating the Browser object
        BrowserPreferences.setChromiumSwitches("--ignore-gpu-blacklist");

        // On Mac OS X Chromium engine must be initialized in non-UI thread.
        if (Environment.isMac()) {

            BrowserCore.initialize();
        }
        loadConfig();
        //
        initLogger();
        //
        setLogLevel(_logLevel);

        // run ble and static server
        mesBridge = new MesServicesBridge(this);
        boolean startedOk = mesBridge.startupSequence();
        if(!startedOk){
            stopLoadingMes();
            return;
        }
        // js bridge is connection between js app and java webview
        this.jsb =  new JsBridge(mesBridge, this);
        // jx browser config and instantiation
        startBrowser(_browserType);
    }

    @Override
    public void start(Stage primaryStage) {

//        loadConfig();
//        //
//        initLogger();
//        //
//        setLogLevel(_logLevel);
//
//        // run ble and static server
//        mesBridge = new MesServicesBridge(this);
//        boolean startedOk = mesBridge.startupSequence();
//        if(!startedOk){
//            stopLoadingMes();
//            return;
//        }
//        // js bridge is connection between js app and java webview
//        this.jsb =  new JsBridge(mesBridge, this);
//        // jx browser config and instantiation
//        startBrowser(_browserType);
        // JavaFx Stuff
        showMainStage(primaryStage);

        String url = Constants.GUI_ENTRYPOINT_BASE_URL + _guiEntryPointUrl;
        // TODO MOVE TO CONFIG?
//        browser.getCacheStorage().clearCache();
        browser.loadURL(url);
        setBrowserListeners();
        setupLoadingTimer();

    }

    private void startBrowser(String bType){

        BrowserType browserType = (bType.equals("light") ? BrowserType.LIGHTWEIGHT : BrowserType.HEAVYWEIGHT);
        browser = new Browser(browserType);

        BrowserPreferences preferences = browser.getPreferences();
        preferences.setJavaScriptEnabled(true);
        preferences.setAllowRunningInsecureContent(true);
        preferences.setPluginsEnabled(true);

    }

    private void setLogLevel(String logLevel){

        if (logLevel.equals("info")){

            LOGGER.setLevel(Level.INFO);
        }else{

            LOGGER.setLevel(Level.SEVERE);
        }
    }

    private void setupLoadingTimer(){

        _loadingTimeout = new MesTimerTask(this, "startup-check");
        Timer t = new Timer();
        t.schedule(_loadingTimeout, (30*1000));
    }

    private void loadConfig(){

        org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
        try {
            Object obj = parser.parse(new FileReader( System.getProperty("user.dir")+ Constants.LOCAL_SLASH_CHAR+Constants.JAR_NAME+".config"));
            JSONObject jsonObject = (JSONObject)obj;
            _iconUrl = (String)jsonObject.getOrDefault("iconFileName", "");
            _guiEntryPointUrl = (String)jsonObject.getOrDefault("guiEntryPointUrl", "");
            _browserType = (String)jsonObject.getOrDefault("browserType", "");
            _logLevel =  (String)jsonObject.getOrDefault("logLevel", "");
            _imageFileName = (String)jsonObject.getOrDefault("imageFileName", "");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.severe(e.getMessage());
        }
    }

    private void stopLoadingMes(){
        LOGGER.info("stopLoadingMes");
        // show close dialog
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("M.E.S.");
        // since a timeout occurred, we want to fetch clues about the problem...
        String errorTitle = "Unknown";
        if(browser != null){
            JSValue document = browser.executeJavaScriptAndReturnValue("window");
            JSValue errorValue = document.asObject().getProperty("WebGLError");
            errorTitle = errorValue.getBooleanValue() ? "WebGL" : "Unknown" ;
        }

        alert.setHeaderText("Failed to initialize the UI ("+errorTitle+" error). Try again or contact support.");

        Optional<ButtonType> result = alert.showAndWait();
        doShutDown();
    }

    private void showMainStage(Stage primaryStage){

        Image image = null;
        try {
            String u =getClass().getResource(this._imageFileName).toURI().toString();
            image = new Image(u);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            LOGGER.severe(e.getMessage());
        }
        _splashImageView = new ImageView();
        _splashImageView.setImage(image);


        BrowserView browserView = new BrowserView(browser);
        StackPane root = new StackPane();

        root.getChildren().addAll(browserView, _splashImageView);
        Scene scene = new Scene(root,  1024, 768);
        primaryStage.setTitle("MES - Meccano Education Software");
        primaryStage.getIcons().add(new Image(_iconUrl));
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);

        primaryStage.setOnCloseRequest(e -> {
            e.consume();
            onCloseWindow(primaryStage);
        });
        primaryStage.show();

    }

    private void setBrowserListeners(){
        // display js logs in Java system.out
        browser.addConsoleListener(new ConsoleListener() {
            public void onMessage(ConsoleEvent event) {

                out.println("Message: " + event.getMessage());
                LOGGER.info(event.getMessage());
            }
        });

        // jsb allows js to call java...
        browser.addScriptContextListener(new ScriptContextAdapter() {
            @Override
            public void onScriptContextCreated(ScriptContextEvent event) {
                Browser browser = event.getBrowser();
                JSValue window = browser.executeJavaScriptAndReturnValue("window");
                window.asObject().setProperty("java", jsb);
            }
        });
    }

    private void initLogger(){
        // This block configure the logger with handler and formatter
        try {
            File dir = new File(Constants.LOG_DIR_PATH);
            dir.mkdirs();
            fh = new FileHandler(Constants.LOG_PATH);

        } catch (IOException e) {
            e.printStackTrace();
            //NO logger yet! LOGGER.severe(e.getMessage());
        }
        LOGGER.addHandler(fh);
        SimpleFormatter formatter = new MesFormatter();

        fh.setFormatter(formatter);
    }

    public static void main(String[] args) {


        launch(args);

    }


    private void onCloseWindow(Stage primaryStage){


        // show close dialog
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("M.E.S.");
        alert.setHeaderText("Unsaved changes will be lost, are you sure?");
        alert.initOwner( primaryStage);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() != ButtonType.OK){
            return;
        }

        callJs("onShutDown");
        mesBridge.shutDownSequence();
        MesTimerTask shutdownTimeout = new MesTimerTask(this, "shut-down-wait");
        Timer t = new Timer();
        t.schedule(shutdownTimeout, (1*2000));
    }

    @Override
    public void onJsLoaded() {
        _isJsLoaded = true;
    }

    @Override
    public boolean isJsLoaded() {
        return _isJsLoaded;
    }

    @Override
    public void onJsTimeOut() {
        LOGGER.info("Waited in vain for js to load... exiting");

        Platform.runLater(new Runnable(){

            @Override
            public void run() {

                stopLoadingMes();
            }
        });
    }

    @Override
    public void callJs(String method) {
        browser.executeJavaScript(method + "();");
    }

    @Override
    public void doShutDown() {
        if(!Environment.isMac()) {
            com.sun.javafx.application.PlatformImpl.tkExit();
        }
        Platform.exit();
        System.exit(0);
    }

    @Override
    public void logInfo(String msg) {
        LOGGER.info(msg);
    }

    @Override
    public void openImageInSystemBrowser(String dataUri, String fileName) {
//        LOGGER.info("openImageInSystemBrowser ::   " + dataUri);

        try{
            byte[] decodedImg = Base64.getDecoder().decode(dataUri.getBytes(StandardCharsets.UTF_8));
            Path destinationFile = Paths.get(Constants.LOG_DIR_PATH, "copied_image-" + System.currentTimeMillis() + ".jpg");
            Files.write(destinationFile, decodedImg);
            getHostServices().showDocument("file:"+destinationFile.toFile());
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
        }
    }
}
