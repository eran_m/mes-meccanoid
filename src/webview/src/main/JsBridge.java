package main;

/**
 * Created by zivtaller on 12/01/2017.
 */
public class JsBridge {

    private MesServicesBridge mServices;
    private AppFromJS app;

    public JsBridge(MesServicesBridge ms, AppFromJS app){
        this.mServices = ms;
        this.app = app;
    }

    public void exit(){

//        System.exit(0);
    }

    public void restartBle(){

        mServices.startBle();
    }

    public void logFromJs(String msg){
        System.out.println("Js: " + msg);
//        Main.LOGGER.info("Js: " + msg);
        app.logInfo("Js: " + msg);
    }

    public void onJsLoaded(){

        this.app.onJsLoaded();
    }

    public void openImageInSystemBrowser(String dataUri, String fileName){

        try{
            this.app.openImageInSystemBrowser( dataUri, fileName);
        }catch (Exception e){
            app.logInfo(e.getMessage());
        }

    }
}
