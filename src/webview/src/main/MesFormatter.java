package main;

import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * Created by ziv on 6/6/2017.
 */
public class MesFormatter extends SimpleFormatter {
    @Override
    public String format(LogRecord record) {
        return new java.util.Date() + " " + record.getLevel() + " " + record.getMessage() + "\r\n";
    }
}
