package main;

import java.util.TimerTask;

/**
 * Created by ziv on 5/30/2017.
 */
public class MesTimerTask extends TimerTask {
    private AppFromJS app;
    private String timerType;
    public MesTimerTask (AppFromJS main, String timerT){

        this.timerType = timerT;
        this.app = main;
    }
    @Override
    public void run() {

        switch(timerType){
            case "startup-check":

                if (!app.isJsLoaded()){

                    app.onJsTimeOut();
                }
                break;
            case "shut-down-wait":
                app.doShutDown();
                break;
        }

    }
}
