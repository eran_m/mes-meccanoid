package main;

/**
 * Created by ziv on 5/11/2017.
 */
public class MesService {

    private String name;
    private int status;
    private String port;

    public String getRunPath() {

        return runPath;
    }

    public void setRunPath(String runPath) {
        this.runPath = runPath;
    }

    private String runPath;

    public MesService(String name, int status, String port, String runPath) {
        this.name = name;
        this.status = status;
        this.port = port;
        this.runPath = runPath;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
