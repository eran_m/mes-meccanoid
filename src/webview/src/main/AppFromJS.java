package main;

/**
 * Created by ziv on 5/17/2017.
 */
public interface AppFromJS {
    public void onJsLoaded();
    public boolean isJsLoaded();
    public void onJsTimeOut();
    public void callJs(String method);
    public void doShutDown();
    public void logInfo(String msg);
    public void openImageInSystemBrowser(String dataUri, String fileName);
}
