package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//import static Main.LOGGER;

/**
 * Created by ziv on 3/6/2017.
 * run the node app async.
 * send output to java console
 */
public class MeccCliUtils {

    private Server myServer;
//    protected String appUrl;
    private AppFromJS app;

    public MeccCliUtils(AppFromJS m){
        this.app = m;
    }
    interface CliCb {
        void onOutput(String msg);
    }

    public void runApp(String path){

        myServer = new Server(path);

        Thread t1 = new Thread(myServer, "T1");
        t1.setDaemon(true);
        t1.start();

    }

    public void stopApp(){
        myServer.stop();
    }
    class Server implements Runnable{

        private String path;
        private volatile boolean exit = false;
        private Process process = null;
        Server(String p){ this.path = p;}

        public void run() {

            String[] l = new String[1];
            l[0] = this.path; //  ".\\src\\ble\\app\\start_from_j.bat";
            app.logInfo("trying to run "+ this.path);

            try {
                process = Runtime.getRuntime().exec(l);
            } catch (IOException e) {
                e.printStackTrace();
                app.logInfo(e.getMessage());
//                LOGGER.severe(e.getMessage());
            }
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String out = "";

                // Read stream here
                while (process.isAlive() && !exit){
                    String line = reader.readLine();
                    if(line != null){
                        out += line + "\n";
                        System.out.println(line);
                    }
                }
            } catch (Exception e) {
                System.out.println(e.toString());
                app.logInfo(e.getMessage());
//                LOGGER.severe(e.getMessage());
            }
        }

        public void stop(){
            exit = true;
            process.destroyForcibly();
        }
    }
}
