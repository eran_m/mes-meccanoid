package main;

import com.teamdev.jxbrowser.chromium.internal.Environment;



public class Constants {

    private static String getJarName() {
        String jarName = new java.io.File(Constants.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
        if (jarName.contains(".")) {
            jarName = jarName.substring(0, jarName.lastIndexOf('.'));
        }
        return jarName;
    }
    public static final String JAR_NAME = getJarName();

    private String slash = "";

    public static final String LOCAL_SLASH_CHAR =  (Environment.isMac()) ? "/" : "\\";
    public static final String SHELL_SUFFIX =  (Environment.isMac()) ? ".sh" : ".bat";
    public static final String BLE_RUNNER_PATH = "."+LOCAL_SLASH_CHAR+"ble_runner"+SHELL_SUFFIX;
    public static final String STATIC_RUNNER_PATH = "."+LOCAL_SLASH_CHAR+"static_runner"+SHELL_SUFFIX;
    public static final String WIN_LOG_DIR_PATH = System.getenv("APPDATA") + LOCAL_SLASH_CHAR +"SpinMaster" + LOCAL_SLASH_CHAR;//"%T\\mes\\";
    public static final String MAC_LOG_DIR_PATH = System.getProperty("user.home")+ "/Library/Logs/SpinMaster/";

    public static final String LOG_DIR_PATH = (Environment.isMac()) ? MAC_LOG_DIR_PATH : WIN_LOG_DIR_PATH;
    public static final String LOG_PATH = LOG_DIR_PATH + "meccanoid_java.log";

    public static final int SERVER_STATUS_UNDEFINED = -1;
    public static final int STATIC_WAS_NOT_RUNNING_AT_STARTUP = 0; // means server was not running when webview loaded
    public static final int STATIC_ALREADY_RUNNING_AT_STARTUP = 1; // means server was running (presumably by another app) when webview loaded
    public static final int BLE_WAS_NOT_RUNNING_AT_STARTUP = 0; // means server was not running when webview loaded
    public static final int BLE_ALREADY_RUNNING_AT_STARTUP = 1; // means server was running (presumably by another app) when webview loaded
    public static final String GUI_ENTRYPOINT_BASE_URL = "http://localhost:5678/snap/";//snap_meccanoid.html";
}
