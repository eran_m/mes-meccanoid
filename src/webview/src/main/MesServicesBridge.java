package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Thread.sleep;
//import static Main.LOGGER;

/**
 * Created by ziv on 5/11/2017.
 */
public class MesServicesBridge {

    private MesService bleServer;
    private MesService staticServer;
    private MeccCliUtils cliRunner;
    private AppFromJS app;


    public MesServicesBridge(AppFromJS main)
    {

        this.cliRunner = new MeccCliUtils(main);
        this.app = main;
        this.bleServer = new MesService(
                "BLE",
                Constants.SERVER_STATUS_UNDEFINED,
                "1234",
                Constants.BLE_RUNNER_PATH
        );
        this.staticServer = new MesService(
                "STATIC",
                Constants.SERVER_STATUS_UNDEFINED,
                "5678",
                Constants.STATIC_RUNNER_PATH
        );
    }

    public boolean startupSequence() {

        app.logInfo("**** node apps startup sequence called");
        boolean isBleRunning = this.callApp("http://localhost:" + bleServer.getPort() + "/alive");
        if (isBleRunning) {

            app.logInfo("ble was already running");
//            LOGGER.info("ble was already running");
            bleServer.setStatus(Constants.BLE_ALREADY_RUNNING_AT_STARTUP);
        } else {

            app.logInfo("ble was not running, try to startup");
            staticServer.setStatus(Constants.BLE_WAS_NOT_RUNNING_AT_STARTUP);
            // launch ble
            this.startBle();
        }

        boolean isStatRunning = this.callApp("http://localhost:" + staticServer.getPort() + "/alive");
        if (isStatRunning) {

            staticServer.setStatus(Constants.STATIC_ALREADY_RUNNING_AT_STARTUP);
            app.logInfo("static was already running");

        } else {
            staticServer.setStatus(Constants.STATIC_WAS_NOT_RUNNING_AT_STARTUP);
            app.logInfo("static was not running, try to startup");

            // launch static
            cliRunner.runApp(staticServer.getRunPath());
        }

        // sleep a little to allow services to load..
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        // if re-launched - check again if static is alive
        if (!isStatRunning) {

            isStatRunning = this.callApp("http://localhost:" + staticServer.getPort() + "/alive");
        }
        app.logInfo("after wait, static " + (isStatRunning ? "loaded successfully" : "not loaded"));

        // static running is enough
        return isStatRunning;
    }

    public void shutDownSequence() {

        app.logInfo("**** node apps shutDown sequence called");

        this.callApp("http://localhost:" + bleServer.getPort() + "/kill");
        this.callApp("http://localhost:" + staticServer.getPort() + "/kill");

    }

    public void startBle() {
        cliRunner.runApp(bleServer.getRunPath());
    }

    private boolean callApp(String spec) {

        app.logInfo("try calling " + spec);
        URL killerRequest = null;
        try {
            killerRequest = new URL(spec);
            URLConnection yc = killerRequest.openConnection();
            InputStream is = killerRequest.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String result = reader.readLine();
            return true;
        } catch (IOException e1) {

            e1.printStackTrace();
            return false;
        }
    }
}

